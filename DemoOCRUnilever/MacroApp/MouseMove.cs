﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacroApp 
{
    public partial class MouseMove : UserControl
    {
        List<int> PointX = new List<int>();
        List<int> PointY = new List<int>();
        int X;
        int Y;
        int a;
        int b;
        int i = 0;

        public MouseMove()
        {
            InitializeComponent();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if(a!=b)
            {
                Cursor.Position = new Point(PointX[a], PointY[a]);
                a++;
                //Cursor.Position = new Point(int.Parse(listView1.Items[a].SubItems[0].Text), int.Parse(listView1.Items[a].SubItems[1].Text));
                //a++;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            X = Cursor.Position.X;
            PointX.Add(X);
            Y = Cursor.Position.Y;
            PointY.Add(Y);
            b++;
            //lv = new ListViewItem(Cursor.Position.X.ToString());
            //lv.SubItems.Add(Cursor.Position.Y.ToString());
            //listView1.Items.Add(lv);
            //b++;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            a = 0;
            b = 0;
            PointX.Clear();
            PointY.Clear();
            timer1.Start();
            timer2.Stop();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            timer2.Stop();
        }

        private void AutoKeyRecord_Load(object sender, EventArgs e)
        {
            a = 0;
            b = 0;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer2.Start();
            a = 0;
        }
    }
}
