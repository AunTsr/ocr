﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using Gma.System.MouseKeyHook;
using MouseKeyboardLibrary;


namespace MacroApp
{
    public partial class MouseClick : UserControl
    {
        bool detect;

        public MouseClick()
        {
            InitializeComponent();
        }

        [DllImport("user32.dll")]
        static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);

        const byte VK_RETURN = 0x0D;

        const uint KEYEVENTF_KEYUP = 0x0002;

        public const int MOUSEEVENTF_LEFTDOWN = 0x02;
        public const int MOUSEEVENTF_LEFTUP = 0x04;
        public const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        public const int MOUSEEVENTF_RIGHTUP = 0x10;

        private void button2_Click(object sender, EventArgs e)
        {
            detect = true;
            while (detect)
            {

            }
            //int X = int.Parse( textBox1.Text);
            //int Y = int.Parse( textBox2.Text);
            //LeftClick(X, Y);
        }

        public static void LeftClick(int x, int y)
        {
            Cursor.Position = new System.Drawing.Point(x, y);
            mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
            write();
            
        }

        private static void write()
        {
            keybd_event(VK_RETURN, 0, 0, 0);
            Thread.Sleep(100);
            keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);
        }


        //private static void Mousedetect()
        //{
        //    Hook.GlobalEvents().MouseDown += async (sender, e) =>
        //    {
        //        int x = Cursor.Position.X;
        //    };
        //}
    }
}
