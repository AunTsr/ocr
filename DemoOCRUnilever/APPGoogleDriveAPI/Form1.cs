﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APPGoogleDriveAPI
{
    public partial class Form1 : Form
    {
        private static string _FolderID;
        private static string[] Scopes = { DriveService.Scope.Drive };
        private static string _ApplicationName = "AunApiForDrive";
        private string _fileName;
        private string _filePath;
        private string _contentType;
        private string _downloadFilePath;
        private static UserCredential credential;
        private static DriveService service;

        public Form1()
        {
            InitializeComponent();
        }

        private void SetContentType (string content)
        {
            switch (content)
            {
                case "xls":
                    _contentType = "application/vnd.ms-excel";
                    break;
                case "xlsx":
                    _contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case "xml":
                    _contentType = "text/xml";
                    break;
                case "ods":
                    _contentType = "application/vnd.oasis.opendocument.spreadsheet";
                    break;
                case "csv":
                    _contentType = "text/plain";
                    break;
                case "tmpl":
                    _contentType = "text/plain";
                    break;
                case "pdf":
                    _contentType = "application/pdf";
                    break;
                case "php":
                    _contentType = "application/x-httpd-php";
                    break;
                case "jpg":
                    _contentType = "image/jpeg";
                    break;
                case "png":
                    _contentType = "image/png";
                    break;
                case "gif":
                    _contentType = "image/gif";
                    break;
                case "bmp":
                    _contentType = "image/bmp";
                    break;
                case "txt":
                    _contentType = "text/plain";
                    break;
                case "doc":
                    _contentType = "application/msword";
                    break;
                case "js":
                    _contentType = "text/js";
                    break;
                case "swf":
                    _contentType = "application/x-shockwave-flash";
                    break;
                case "mp3":
                    _contentType = "audio/mpeg";
                    break;
                case "zip":
                    _contentType = "application/zip";
                    break;
                case "rar":
                    _contentType = "application/rar";
                    break;
                case "tar":
                    _contentType = "application/tar";
                    break;
                case "arj":
                    _contentType = "application/arj";
                    break;
                case "cab":
                    _contentType = "application/cab";
                    break;
                case "html":
                    _contentType = "text/html";
                    break;
                case "htm":                    
                    _contentType = "text/html";
                    break;
                case "default":
                    _contentType = "application/octet-stream";
                    break;
                case "folder":
                    _contentType = "application/vnd.google-apps.folder";
                    break;
            }
        }

        private static string UploadFileToDrive(DriveService service, string filename, string filePath, string contentType)
        {
            var fileMatadata = new Google.Apis.Drive.v3.Data.File();
            fileMatadata.Name = filename;
            fileMatadata.Parents = new List<string> { _FolderID };

            FilesResource.CreateMediaUpload request;

            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                request = service.Files.Create(fileMatadata, stream, contentType);
                request.Upload();
            }
            var file = request.ResponseBody;

            return file.Id;
        }

        private static void DownloadFileFormDrive(DriveService service, string fileId, string filePath)
        {
            var request = service.Files.Get(fileId);

            using (var memoryStream = new MemoryStream())
            {
                request.MediaDownloader.ProgressChanged += (IDownloadProgress progress) =>
                {
                    switch (progress.Status)
                    {
                        case DownloadStatus.Downloading:
                            MessageBox.Show(progress.BytesDownloaded.ToString());
                            break;
                        case DownloadStatus.Completed:
                            MessageBox.Show("Download complete");
                            break;
                        case DownloadStatus.Failed:
                            MessageBox.Show("Download failed");
                            break;
                    }
                };

                request.Download(memoryStream);

                using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                {
                    fileStream.Write(memoryStream.GetBuffer(), 0, memoryStream.GetBuffer().Length);
                }
            }
        }

        private void Upload_Click(object sender, EventArgs e)
        {
            _fileName = textBox1.Text;
            _filePath = textBox2.Text;
            SetContentType(comboBox1.Text);
            UploadFileToDrive(service, _fileName, _filePath, _contentType);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            credential = ServiceAPI.GetUserCredential();
            service = ServiceAPI.GetDriveService(credential, _ApplicationName);
        }

        private void Download_Click(object sender, EventArgs e)
        {
            _FolderID = textBox4.Text;
            _downloadFilePath = textBox3.Text;
            DownloadFileFormDrive(service, _FolderID, _downloadFilePath);
        }
    }
}
