﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace APPGoogleDriveAPI
{
    public static class ServiceAPI
    {
        public static string[] Scopes = { DriveService.Scope.Drive };
        public static UserCredential GetUserCredential()
        {
            using (var stream = new FileStream(@"C:\Users\prata\source\repos\DemoOCRUnilever\APPGoogleDriveAPI\bin\Debug\client_secret_Aun.json", FileMode.Open, FileAccess.Read))
            {
                string creadPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                creadPath = Path.Combine(creadPath, "driveApiCredentials", "drive-credentials.json");

                return GoogleWebAuthorizationBroker.AuthorizeAsync(GoogleClientSecrets.Load(stream).Secrets, Scopes, "User", CancellationToken.None, new FileDataStore(creadPath, true)).Result;
            }
        }

        public static DriveService GetDriveService(UserCredential credential, string _ApplicationName)
        {
            return new DriveService(
                new BaseClientService.Initializer
                {
                    HttpClientInitializer = credential,
                    ApplicationName = _ApplicationName
                });
        }

    }
}
