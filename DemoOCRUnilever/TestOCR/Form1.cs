﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Emgu;

namespace TestOCR
{
    public partial class Form1 : Form
    {
        int yy = 0;
        string ImgLOcation;
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    ImgLOcation = openFileDialog1.FileName;
                    pictureBox1.ImageLocation = ImgLOcation;
                    //imageBox1.BackgroundImage.
                }
                catch (Exception Ex)
                {

                }
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Bitmap IMG = new Bitmap(ImgLOcation);
            Bitmap temp = Con2(IMG);
            Bitmap bitcycle = EdgeCycle(IMG);
            Bitmap bitmap = EdgeDetect(IMG);
            pictureBox2.Image = bitcycle;
        }

        private Bitmap Con2(Bitmap Img)
        {
            Bitmap aa = (Bitmap)Img.Clone();
            int x, y;

            // Loop through the images pixels to reset color.
            for (x = 0; x < Img.Width; x++)
            {
                for (y = 0; y < Img.Height; y++)
                {
                    Color pixelColor = Img.GetPixel(x, y);
                    int R = pixelColor.R;
                    int G = pixelColor.G;
                    int B = pixelColor.B;
                    int avr = (R + G + B) / 3;
                    //Color newColor = Color.FromArgb(pixelColor.R, 128, 128);
                    aa.SetPixel(Img.Width - x - 1, y, Color.FromArgb(avr, avr, avr)); // Now greyscale
                }
            }
            return aa;
        }

        private Bitmap EdgeCycle(Bitmap bitmap)
        {
            Bitmap aa = (Bitmap)bitmap.Clone();
            int x, y;
            int LimitL = 255; int LimitR = 255;
            int avrL = 0; int avrR = 0;
            int XL = 0;int XR = 0;int YL = 0;int YR=0;

            // Loop through the images pixels to reset color.
            for (y = 0; y < bitmap.Height ; y++)
            {
                for (x = 0; x < bitmap.Width/2+1; x++)
                {
                    Color pixelColorL = bitmap.GetPixel(x, y);
                    int RL = pixelColorL.R;
                    int GL = pixelColorL.G;
                    int BL = pixelColorL.B;
                    avrL = (RL + GL + BL) / 3;
                    if (avrL < LimitL+7)
                    {
                        LimitL = avrL;
                        XL = x;
                        YL = y;
                    }
                    Color pixelColorR = bitmap.GetPixel(bitmap.Width-x-1, y);
                    int RR = pixelColorR.R;
                    int GR = pixelColorR.G;
                    int BR = pixelColorR.B;
                    avrR = (RR + GR + BR) / 3;
                    if (avrR < LimitR+7)
                    {
                        LimitR = avrR;
                        XR = bitmap.Width - x - 1;
                        YR = y;
                    }
                    //Color newColor = Color.FromArgb(pixelColor.R, 128, 128);
                    aa.SetPixel(x, y, Color.Transparent); // Now greyscale
                    aa.SetPixel(bitmap.Width-x-1, y, Color.Transparent); // Now greyscale
                }
                aa.SetPixel(XR, YR, Color.Black); // Now greyscale
                aa.SetPixel(XL, YL, Color.Black); // Now greyscale
                XL = 0;
                XR = 0;
                YL = 0;
                YR = 0;

            }
            return aa;
        }

        private Bitmap EdgeDetect(Bitmap bitmap)
        {
            int threshold = 128 * 128;

            int[,] gx = new int[,] { { 1, 0, -1 }, { 2, 0, -2 }, { 1, 0, -1 } };
            int[,] gy = new int[,] { { -1, -2, -1 }, { 0, 0, 0 }, { 1, 2, 1 } };

            int[,] R = new int[bitmap.Width, bitmap.Height];
            int[,] G = new int[bitmap.Width, bitmap.Height];
            int[,] B = new int[bitmap.Width, bitmap.Height];

            Bitmap aa = (Bitmap)bitmap.Clone();

            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Color pixelColor = bitmap.GetPixel(x, y);
                    R[x, y] = pixelColor.R;
                    G[x, y] = pixelColor.G;
                    B[x, y] = pixelColor.B;
                }
            }

            int rrx = 0, rry = 0;
            int ggx = 0, ggy = 0;
            int bbx = 0, bby = 0;
            int rr, gg, bb;

            for (int x = 1; x < bitmap.Width - 1; x++)
            {
                for (int y = 1; y < bitmap.Height - 1; y++)
                {

                    rrx = 0;
                    rry = 0;
                    ggx = 0;
                    ggy = 0;
                    bbx = 0;
                    bby = 0;
                    rr = 0;
                    gg = 0;
                    bb = 0;

                    for (int i = -1; i < 2; i++)
                    {
                        for (int j = -1; j < 2; j++)
                        {
                            rr = R[x + j, y + i];
                            rrx += gx[i + 1, j + 1] * rr;
                            rry += gy[i + 1, j + 1] * rr;

                            gg = G[x + j, y + i];
                            ggx += gx[i + 1, j + 1] * gg;
                            ggy += gy[i + 1, j + 1] * gg;

                            bb = B[x + j, y + i];
                            ggx += gx[i + 1, j + 1] * bb;
                            ggy += gy[i + 1, j + 1] * bb;
                        }
                    }
                    if (rrx * rrx + rry * rry > threshold || ggx * ggx + ggy * ggy > threshold || bbx * bbx + bby * bby > threshold)aa.SetPixel(x, y, Color.Black);
                    else aa.SetPixel(x, y, Color.Transparent);
                }
            }
            return aa;
        }
    }
}
