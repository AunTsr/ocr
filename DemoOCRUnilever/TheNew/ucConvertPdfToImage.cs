﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Drawing;
using System.Drawing.Imaging;
using System.Collections;
using Emgu.CV;
using cs_pdf_to_image;
using PdfToImage;


namespace TheNew
{
    public partial class ucConvertPdfToImage : UserControl
    {
        SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();
        string pdfName;
        string Dpi;

        public  ucConvertPdfToImage()
        {
            InitializeComponent();
        }

        private void OpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pathImage.Text = openFileDialog1.FileName;
            }
        }

        private void pdfToTiff_Click(object sender, EventArgs e)
        {
            pdfName = pathImage.Text;
            Dpi = dpiValue.Text;

            f.OpenPdf(pdfName);
            if (f.PageCount > 0)
            {
                f.ImageOptions.Dpi = int.Parse(Dpi);
                f.ToMultipageTiff($"{pdfName}.tiff");
            }
        }

        private void pdfToJpg_Click(object sender, EventArgs e)
        {
            pdfName = pathImage.Text;
            Dpi = dpiValue.Text;
            string part = Path.GetDirectoryName(pdfName);
            string page = pagePdf.Text;
            string fullPart = part + "\\" + page + ".jpg";           

            f.OpenPdf(pdfName);
            if (f.PageCount > 0)
            {
                f.ImageOptions.Dpi = int.Parse(Dpi);
                f.ImageOptions.ImageFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
                f.ToImage(fullPart, int.Parse(page));
            }
        }
    }
}
