﻿using CnetSDK.PDFtoImage.Converter.Trial;
using iDiTect.Converter;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using org.pdfclown.documents;
using org.pdfclown.files;
using org.pdfclown.tools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;


namespace TheNew
{
    public static class PdfToImageHelper
    {
        public static void Convert()
        {
            //Copy "x86" and "x64" folders from download package to your .NET project Bin folder.
            PdfToImageConverter converter = new PdfToImageConverter();

            converter.Load(System.IO.File.ReadAllBytes("filter2.pdf"));

            //Default is 72, the higher DPI, the bigger size out image will be
            converter.DPI = 96;
            //The value need to be 1-100. If set to 100, the converted image will take the
            //original quality with less time and memory. If set to 1, the converted image 
            //will be compressed to minimum size with more time and memory.
            //converter.CompressedRatio = 80;

            for (int i = 0; i < converter.PageCount; i++)
            {
                //The converted image will keep the original size of PDF page
                Image pageImage = converter.PageToImage(i);
                //To specific the converted image size by width and height
                //Image pageImage = converter.PageToImage(i, 100, 150);
                //You can save this Image object to jpeg, tiff and png format to local file.
                //Or you can make it in memory to other use.
                pageImage.Save(i.ToString() + ".jpg", ImageFormat.Jpeg);
            }

        }

        public static void Convert2()
        {
            //Copy "x86" and "x64" folders from download package to your .NET project Bin folder.
            PdfToImageConverter converter = new PdfToImageConverter();

            //Default is 72, the higher DPI, the bigger size out image will be
            converter.DPI = 96;

            using (Stream stream = System.IO.File.OpenRead("filter2.pdf"))
            {
                converter.Load(stream);
                //Save pdf to multiple pages tiff to local file
                converter.DocumentToMultiPageTiff("convert.tiff");
                //Or save the multiple pages tiff in memory to other use
                //Image multipageTif = converter.DocumentToMultiPageTiff();
            }
        }

        public static void PDFToJPG()
        {
            // Create an instance of CnetSDK.PDFtoImage.Converter.Trial.PDFDocument object.
            PdfFile pdfDoc = new PdfFile();

            // Load PDF document from local file.
            pdfDoc.LoadPdfFile("filter2.pdf");

            // Get the total page count.
            int count = pdfDoc.FilePageCount;

            for (int i = 0; i < count; i++)
            {
                // Convert PDF page to image.
                Bitmap jpgImage = pdfDoc.ConvertToImage(i);

                // Save image with jpg file type.
                jpgImage.Save("CnetSDK" + i + ".jpg", ImageFormat.Jpeg);
            }
        }

        public static void testpdftoimg()
        {
            // 1. Opening the PDF file...
            string filePath = "filter2.pdf";
            using (org.pdfclown.files.File file = new org.pdfclown.files.File(filePath))
            {
                Document document = file.Document;
                Pages pages = document.Pages;

                // 2. Page rasterization.
                int pageIndex = 1;
                Page page = pages[pageIndex];
                SizeF imageSize = page.Size;
                Renderer renderer = new Renderer();
                Image image = renderer.Render(page, imageSize);

                // 3. Save the page image!
                image.Save("ContentRenderingSample.jpg", ImageFormat.Jpeg);
            }
        }
    }
}
