﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace TheNew
{
    public partial class ucReadWriteExcel : UserControl
    {
        string path = "";
        string result = "";
        string page;
        bool color = true;

        public ucReadWriteExcel()
        {
            InitializeComponent();
        }

        private void ReadAll_Click(object sender, EventArgs e)
        {
            page = pageNumber.Text;
            path = AppDomain.CurrentDomain.BaseDirectory + pathName.Text+".xlsx";
            result = fnReadWriteExcel.ReadAllExcel(path, int.Parse(page));
            textBox2.Text = result;
        }

        private void ReadFromCell_Click(object sender, EventArgs e)
        {
            page = pageNumber.Text;
            int row = int.Parse(cellRow.Text);
            int col = int.Parse(cellCol.Text);
            path = AppDomain.CurrentDomain.BaseDirectory + pathName.Text + ".xlsx";
            result = fnReadWriteExcel.ReadRowColExcel(row, col, path, int.Parse(page));
            textBox2.Text = result;
        }

        private void WriteFromCell_Click(object sender, EventArgs e)
        {
            page = pageNumber.Text;
            int row = int.Parse(cellRow.Text);
            int col = int.Parse(cellCol.Text);
            path = AppDomain.CurrentDomain.BaseDirectory + pathName.Text + ".xlsx";
            string Data = textBox2.Text;
            fnReadWriteExcel.WriteRowColExcel(row, col, Data, int.Parse(page), path, color);
        }
    }
}
