﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CnetSDK.PDFtoImage.Converter.Trial;
using Emgu.CV;
using Emgu.CV.Structure;
using Microsoft.Office.Interop.Excel;
using Apitron.PDF.Rasterizer;

namespace TheNew
{
    public partial class Form1 : Form
    {
        List<Color> ramdomColors = new List<Color>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        //    var unnone = int.Parse(textBox1.Text);
        //    var aa = unnone.GetType();
        //    textBox2.Text = aa.Name.ToString();

            string s = textBox1.Text;
            byte[] bytes = Encoding.ASCII.GetBytes(s);
            List<int> ints = new List<int>();
            //int a = BitConverter.ToInt32(bytes, 0);

            for (int i = 0; i < bytes.Length;i++)
            {
                ints.Add((int)bytes[i]);
            }
            //int aa = BitConverter.ToInt32(bytes, 0);
            //string AsciiNumber = bytes.ToString();
            //int result = BitConverter.ToInt32(bytes, 0);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                PdfFile PDFtoImageConverter = new PdfFile();
                PDFtoImageConverter.LoadPdfFile(openFileDialog1.FileName);
                int Count = PDFtoImageConverter.FilePageCount;
                PDFtoImageConverter.SetDPI = 150;
                for (int i = 0; i < Count; i++)
                {
                    Bitmap bmp = PDFtoImageConverter.ConvertToImage(i, 768, 1024);
                    bmp.Save(openFileDialog1.FileName + i + ".jpeg", ImageFormat.Jpeg);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PdfToImageHelper.testpdftoimg();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            List<string> a = new List<string>();
            a.Add("");
            a.Add("");
            a.Add("");
            a.Add("");
            a.Add("");
            a.Add("");
            int t = a.Count;
            a[0] = "00";
            a[1] = "11";
            a[2] = "22";
            a[3] = "33";
            //a[4] = "44";
            a.RemoveAt(1);
            a.RemoveAt(1);
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            Random randonGen = new Random();
            Color randomColor = Color.FromArgb(randonGen.Next(255), randonGen.Next(255),
            randonGen.Next(255));
            ramdomColors.Add(randomColor);
        }
    }
}
