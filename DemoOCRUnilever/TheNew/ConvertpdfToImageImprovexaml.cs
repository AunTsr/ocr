﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Ghostscript.NET.Rasterizer;
using System.Drawing.Imaging;
using System.IO;

namespace TheNew
{
    public partial class ConvertpdfToImageImprovexaml : UserControl
    {
        public ConvertpdfToImageImprovexaml()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog(); 
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string PdfFile = openFileDialog1.FileName;
                    string PngFile = "Convert.png";
                    //PdfToImage.PDFConvert pdfConvert = new PdfToImage.PDFConvert();
                    //pdfConvert.Convert(PdfFile, PngFile, "300");
                    List<string> Convertion = cs_pdf_to_image.Pdf2Image.Convert(PdfFile, PngFile);
                    Bitmap Output = new Bitmap(PngFile);
                    Mat img = new Mat();
                    img = ConvertImage.BitmapToMat(Output);
                    CvInvoke.Imwrite("TT.png",img);
                    //CvInvoke.Imshow("Test", img);
                }
                catch (Exception E)
                {
                    MessageBox.Show(E.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string PdfFile = openFileDialog1.FileName;
                var fileName = "aaa";
                    PdfToPng(PdfFile, fileName);
            }
        }

        private  void PdfToPng(string inputFile, string outputFileName)
        {
            string outputFolder = AppDomain.CurrentDomain.BaseDirectory;
            var xDpi = 100; //set the x DPI
            var yDpi = 100; //set the y DPI
            var pageNumber = 1; // the pages in a PDF document



            using (GhostscriptRasterizer rasterizer = new GhostscriptRasterizer()) //create an instance for GhostscriptRasterizer
            {
                rasterizer.Open(inputFile); //opens the PDF file for rasterizing

                //set the output image(png's) complete path
                var outputPNGPath = Path.Combine(outputFolder, string.Format("{0}.png", outputFileName));

                //converts the PDF pages to png's 
                var pdf2PNG = rasterizer.GetPage(xDpi, yDpi, pageNumber);

                //save the png's
                pdf2PNG.Save(outputPNGPath, ImageFormat.Png);

            }
        }
    }
}
