﻿namespace TheNew
{
    partial class ucConvertPdfToImage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pathImage = new System.Windows.Forms.TextBox();
            this.dpiValue = new System.Windows.Forms.TextBox();
            this.OpenFile = new System.Windows.Forms.Button();
            this.PdfToTiff = new System.Windows.Forms.Button();
            this.PdfToJpg = new System.Windows.Forms.Button();
            this.pagePdf = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pathImage
            // 
            this.pathImage.Location = new System.Drawing.Point(36, 47);
            this.pathImage.Name = "pathImage";
            this.pathImage.Size = new System.Drawing.Size(239, 20);
            this.pathImage.TabIndex = 0;
            // 
            // dpiValue
            // 
            this.dpiValue.Location = new System.Drawing.Point(36, 86);
            this.dpiValue.Name = "dpiValue";
            this.dpiValue.Size = new System.Drawing.Size(239, 20);
            this.dpiValue.TabIndex = 1;
            // 
            // OpenFile
            // 
            this.OpenFile.Location = new System.Drawing.Point(281, 45);
            this.OpenFile.Name = "OpenFile";
            this.OpenFile.Size = new System.Drawing.Size(75, 100);
            this.OpenFile.TabIndex = 2;
            this.OpenFile.Text = "OpenFilePdf";
            this.OpenFile.UseVisualStyleBackColor = true;
            this.OpenFile.Click += new System.EventHandler(this.OpenFile_Click);
            // 
            // PdfToTiff
            // 
            this.PdfToTiff.Location = new System.Drawing.Point(36, 161);
            this.PdfToTiff.Name = "PdfToTiff";
            this.PdfToTiff.Size = new System.Drawing.Size(75, 23);
            this.PdfToTiff.TabIndex = 3;
            this.PdfToTiff.Text = "PdfToTiff";
            this.PdfToTiff.UseVisualStyleBackColor = true;
            this.PdfToTiff.Click += new System.EventHandler(this.pdfToTiff_Click);
            // 
            // PdfToJpg
            // 
            this.PdfToJpg.Location = new System.Drawing.Point(179, 161);
            this.PdfToJpg.Name = "PdfToJpg";
            this.PdfToJpg.Size = new System.Drawing.Size(75, 23);
            this.PdfToJpg.TabIndex = 4;
            this.PdfToJpg.Text = "PdfToJpg";
            this.PdfToJpg.UseVisualStyleBackColor = true;
            this.PdfToJpg.Click += new System.EventHandler(this.pdfToJpg_Click);
            // 
            // pagePdf
            // 
            this.pagePdf.Location = new System.Drawing.Point(36, 125);
            this.pagePdf.Name = "pagePdf";
            this.pagePdf.Size = new System.Drawing.Size(239, 20);
            this.pagePdf.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "PartFile";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Dpi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Page";
            // 
            // ucConvertPdfToImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pagePdf);
            this.Controls.Add(this.PdfToJpg);
            this.Controls.Add(this.PdfToTiff);
            this.Controls.Add(this.OpenFile);
            this.Controls.Add(this.dpiValue);
            this.Controls.Add(this.pathImage);
            this.Name = "ucConvertPdfToImage";
            this.Size = new System.Drawing.Size(384, 200);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox pathImage;
        private System.Windows.Forms.TextBox dpiValue;
        private System.Windows.Forms.Button OpenFile;
        private System.Windows.Forms.Button PdfToTiff;
        private System.Windows.Forms.Button PdfToJpg;
        private System.Windows.Forms.TextBox pagePdf;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}
