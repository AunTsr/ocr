﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;

namespace TheNew
{
    public static class fnReadWriteExcel
    {
        public static string path = "";
        public static string result = "";
        public static _Excel.Application excel = new _Excel.Application();
        public static Workbook wb;
        public static Worksheet ws;
        public static Range rang;

        public static string ReadAllExcel(string path, int page)
        {
            ReadExcel(path, page);
            for (int i = 1; i <= rang.Rows.Count; i++)
            {
                for (int j = 1; j <= rang.Columns.Count; j++)
                {
                    if (rang.Cells[i, j] != null && rang.Cells[i, j].Value2 != null)
                        result = result + rang.Cells[i, j].Value2.ToString() + " ";
                }
                result = result + "\r\n";
            }
            //Marshal.ReleaseComObject(ws);
            wb.Close();
            return result;            
        }

        public static string ReadRowColExcel(int row, int col, string path, int page)
        {
            ReadExcel(path, page);
            row++;
            col++;
            if (rang.Cells[row, col].Value2 != null)
            {
                result = rang.Cells[row, col].Value2.ToString();
            }
            else
            {
                result = "";
            }
            //Marshal.ReleaseComObject(ws);
            wb.Close();
            return result;
        }

        public static void ReadExcel(string path, int page)
        {
            try
            {
                wb = excel.Workbooks.Open(path);
                ws = wb.Sheets[page];
                rang = ws.UsedRange;
            }
            catch
            {
                CreateNewFile(page);
            }
        }

        public static void CreateNewFile(int page)
        {
            wb = excel.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            ws = wb.Worksheets[1];
            rang = ws.UsedRange;
            for (int i = 1; i < page; i++)
            {
                CreateNewSheet();
            }
            ws = wb.Worksheets[page];            
        }

        public static void CreateNewSheet()
        {
            Worksheet aa = wb.Worksheets.Add(After: ws);
        }

        public static void WriteRowColExcel(int row, int col, string Data, int page, string path, bool color)
        {
            ReadExcel(path, page);
            row++;
            col++;
            rang.Cells[row, col].Font.Color = Color.Green;
            rang.Cells[row, col].Value2 = Data;
            wb.SaveAs(path);
            //Marshal.ReleaseComObject(ws);
            //wb.Close();
            ClearUp();
        }

        public static void Setcolor(int row, int col, bool color)
        {
            if (color==true)
            {
                rang.Cells[row, col].Font.Color = Color.Black;
            }
            else if (color==false)
            {
                rang.Cells[row, col].Font.Color = Color.Red;
            }
        }
        private static void ClearUp()
        {
            // Cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            Marshal.FinalReleaseComObject(rang);
            Marshal.FinalReleaseComObject(ws);

            wb.Close(Type.Missing, Type.Missing, Type.Missing);
            Marshal.FinalReleaseComObject(wb);

            excel.Quit();
            Marshal.FinalReleaseComObject(excel);
        }
    }
}
