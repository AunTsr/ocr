﻿using System;
using System.Windows.Forms;

namespace Macro
{
    /// <summary>
    /// All possible events that macro can record
    /// </summary>
    [Serializable]
    public enum MacroEventType
    {
        MouseMove,
        MouseDown,
        MouseUp,
        MouseWheel,
        KeyDown,
        KeyUp
    }

    public class MymacroData
    {
        public int Step { get; set; }
        public int Delay { get; set; }
        public MacroEventType MacroEvent { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public MouseButtons Button { get; set; }
        public Keys Keycode { get; set; }

        public MymacroData()
        {

        }
    }
}
