﻿using Microsoft.VisualBasic;
using MouseKeyboardLibrary;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Macro
{
    public partial class Form1 : Form
    {
        List<MymacroData> dt = new List<MymacroData>();
        List<int> Steps = new List<int>();
        List<MacroEventType> Events = new List<MacroEventType>(); 
         List<Keys> KeyCodes = new List<Keys>();
        List<int> Xs = new List<int>();
        List<int> Ys = new List<int>();
        List<int> Delays = new List<int>();
        List<MouseButtons> Buttons = new List<MouseButtons>();
        int lastTimeRecorded = 0;
        int stps = 0;
        int z = 0;
        int TT;
        int InsertRow;
        const int Delay = 100;
        MouseHook mouseHook = new MouseHook();
        KeyboardHook keyboardHook = new KeyboardHook();


        public Form1()
        {
            InitializeComponent();

            mouseHook.MouseDown += new MouseEventHandler(mouseHook_MouseDown);
            mouseHook.MouseUp += new MouseEventHandler(mouseHook_MouseUp);

            keyboardHook.KeyDown += new KeyEventHandler(keyboardHook_KeyDown);
            keyboardHook.KeyUp += new KeyEventHandler(keyboardHook_KeyUp);
        }

        void mouseHook_MouseDown(object sender, MouseEventArgs e)
        {
            stps++;
            dt.Add(new MymacroData()
            {
                Step = stps,
                Delay = Delay,
                MacroEvent = MacroEventType.MouseDown,
                X = e.X,
                Y = e.Y,
                Button = e.Button
            }                             
            );
            lastTimeRecorded = Environment.TickCount;
        }

        void mouseHook_MouseUp(object sender, MouseEventArgs e)
        {
            stps++;
            dt.Add(new MymacroData()
            {
                Step = stps,
                Delay = Delay,
                MacroEvent = MacroEventType.MouseUp,
                X = e.X,
                Y = e.Y,
                Button = e.Button
            }
            );
            lastTimeRecorded = Environment.TickCount;
        }

        void keyboardHook_KeyDown(object sender, KeyEventArgs e)
        {
            stps++;
            dt.Add(new MymacroData()
            {
                Step = stps,
                Delay = Delay,
                MacroEvent = MacroEventType.KeyDown,
                Keycode = e.KeyCode
            }
            );
            lastTimeRecorded = Environment.TickCount;
        }

        void keyboardHook_KeyUp(object sender, KeyEventArgs e)
        {
            stps++;
            dt.Add(new MymacroData()
            {
                Step = stps,
                Delay = Delay,
                MacroEvent = MacroEventType.KeyUp,
                Keycode = e.KeyCode
            }
            );
            lastTimeRecorded = Environment.TickCount;
        }

        private void btRecord_Click(object sender, EventArgs e)
        {
            btRecord.BackColor = Color.LimeGreen;
            dt.Clear();
            lastTimeRecorded = Environment.TickCount;

            keyboardHook.Start();
            mouseHook.Start();
        }

        private void btStop_Click(object sender, EventArgs e)
        {
            keyboardHook.Stop();
            mouseHook.Stop();
            setDt2Datagrid();
            btRecord.BackColor = SystemColors.Control;
        }

        private void btPlay_Click(object sender, EventArgs e)
        {
            foreach (MymacroData macroEvent in dt)
            {
                Thread.Sleep(macroEvent.Delay);

                switch (macroEvent.MacroEvent)
                {
                    case MacroEventType.MouseMove:
                        {

                            //MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;

                            MouseSimulator.X = macroEvent.X;
                            MouseSimulator.Y = macroEvent.Y;

                        }
                        break;
                    case MacroEventType.MouseDown:
                        {

                            //MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;

                            MouseSimulator.X = macroEvent.X;
                            MouseSimulator.Y = macroEvent.Y;

                            MouseSimulator.MouseDown(macroEvent.Button);

                        }
                        break;
                    case MacroEventType.MouseUp:
                        {

                            //MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;

                            MouseSimulator.MouseUp(macroEvent.Button);

                        }
                        break;
                    case MacroEventType.KeyDown:
                        {

                            //KeyEventArgs keyArgs = (KeyEventArgs)macroEvent.EventArgs;

                            KeyboardSimulator.KeyDown(macroEvent.Keycode);

                        }
                        break;
                    case MacroEventType.KeyUp:
                        {

                            //KeyEventArgs keyArgs = (KeyEventArgs)macroEvent.EventArgs;

                            KeyboardSimulator.KeyUp(macroEvent.Keycode);

                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public void setDt2Datagrid()
        {
            dataGridView1.Rows.Clear();
            var query1 = dt.Select(S => S.Step);
            Steps = query1.Cast<int>().ToList();
            var query2 = dt.Select(T => T.MacroEvent);
            Events = query2.Cast<MacroEventType>().ToList();
            var query3 = dt.Select(K => K.Keycode);
            KeyCodes = query3.Cast<Keys>().ToList();
            var query4 = dt.Select(X => X.X);
            Xs = query4.Cast<int>().ToList();
            var query5 = dt.Select(Y => Y.Y);
            Ys = query5.Cast<int>().ToList();
            var query6 = dt.Select(D => D.Delay);
            Delays = query6.Cast<int>().ToList();
            var query7 = dt.Select(B => B.Button);
            Buttons = query7.Cast<MouseButtons>().ToList();

            for (int i = 0; i < dt.Count; i++)
            {
                dataGridView1.Rows.Add(new object[] { Steps[i], Events[i], KeyCodes[i], Buttons[i], Xs[i], Ys[i], Delays[i] });
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            int j = 0;
            int k = 0;
            z = 0;
            dataGrid2dt();

            dt = dt.OrderBy(S => S.Step).ToList();

            for (int i=0; i < dt.Count; i++)
            {
                dt[i] = new MymacroData() { Step = i + 1, Delay = Delays[i], MacroEvent = Events[i], X = Xs[i], Y = Ys[i], Button = Buttons[i], Keycode = KeyCodes[i] };            
            }

            var newtemplate = Interaction.InputBox("", "Templatename");
            if (newtemplate.Trim() != "")
            {
                Savejson(newtemplate + ".Macro");
            }
            setDt2Datagrid();
        }

        void Savejson(string name)
        {
            var testjsonsave = SystemData.ObjToJson(dt);
            SystemData.SaveTxt2file(name, testjsonsave);
        }

        private void BtOpen_Click(object sender, EventArgs e)
        {
            dt.Clear();
            dataGridView1.Rows.Clear();

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "(Macro Only)|*.Macro";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var configname = openFileDialog1.FileName;
                var config = SystemData.ReadTxtfile(configname);
                dt = SystemData.JsontoObj<List<MymacroData>>(config);
                setDt2Datagrid();
            }
        }

        private void dataGrid2dt()
        {
            dt.Clear();
            Steps.Clear();
            Events.Clear();
            KeyCodes.Clear();
            Xs.Clear();
            Ys.Clear();
            Delays.Clear();
            Buttons.Clear();

            int t = 0;
            MacroEventType Ma;
            MouseButtons Bt;
            Keys Kc;
            for(int ntrow = 0; ntrow < dataGridView1.Rows.Count - 1; ntrow++)
            {
                var val = dataGridView1.Rows[ntrow].Cells["colStep"].Value;
                if (val!=null)
                {
                    Ma = SetMacroEvent(ntrow);
                    Bt = SetButton(ntrow);
                    Kc = SetKeysCode(ntrow);

                    dt.Add(new MymacroData()
                    {
                        Step = int.Parse(dataGridView1.Rows[ntrow].Cells["colStep"].Value.ToString())-t,
                        Delay = int.Parse(dataGridView1.Rows[ntrow].Cells["ColDelay"].Value.ToString()),
                        X = int.Parse(dataGridView1.Rows[ntrow].Cells["colX"].Value.ToString()),
                        Y = int.Parse(dataGridView1.Rows[ntrow].Cells["colY"].Value.ToString()),
                        MacroEvent = Ma,
                        Button = Bt,
                        Keycode = Kc
                    });

                    Steps.Add(int.Parse(dataGridView1.Rows[ntrow].Cells["colStep"].Value.ToString())-t);
                    Events.Add(Ma);
                    KeyCodes.Add(Kc);
                    Xs.Add(int.Parse(dataGridView1.Rows[ntrow].Cells["colX"].Value.ToString()));
                    Ys.Add(int.Parse(dataGridView1.Rows[ntrow].Cells["colY"].Value.ToString()));
                    Delays.Add(int.Parse(dataGridView1.Rows[ntrow].Cells["ColDelay"].Value.ToString()));
                    Buttons.Add(Bt);
                }

                else if (val==null)
                {
                    t++;
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {            
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                InsertRow = row.Index+1;
            }

            if (InsertRow!=TT)
            {
                TT = InsertRow;
                z = 0;
            }
            z++;                   
            dataGridView1.Rows.Insert(InsertRow);
            for (int i = InsertRow ; i < dataGridView1.Rows.Count - z; i++)
            {
                if (dataGridView1.Rows[i + z].Cells["ColEvent"].Value != null)
                {
                    dataGridView1.Rows[i + z].Cells["colStep"].Value = i + 1 + z;
                }
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dt.Clear();
            Steps.Clear();
            Events.Clear();
            KeyCodes.Clear();
            Xs.Clear();
            Ys.Clear();
            Delays.Clear();
            Buttons.Clear();
            stps = 0;
            z = 0;
        }

        private MacroEventType SetMacroEvent(int ntrow)
        {
            if (dataGridView1.Rows[ntrow].Cells["ColEvent"].Value.ToString() == "MouseDown")
            {
                return MacroEventType.MouseDown;
            }
            else if (dataGridView1.Rows[ntrow].Cells["ColEvent"].Value.ToString() == "MouseUp")
            {
                return MacroEventType.MouseUp;
            }
            else if (dataGridView1.Rows[ntrow].Cells["ColEvent"].Value.ToString() == "KeyDown")
            {
                return MacroEventType.KeyDown;
            }
            else if (dataGridView1.Rows[ntrow].Cells["ColEvent"].Value.ToString() == "KeyUp")
            {
                return MacroEventType.KeyUp;
            }
            else
            {
                return MacroEventType.MouseWheel;
            }
        }

        private MouseButtons SetButton(int ntrow)
        {
            if (dataGridView1.Rows[ntrow].Cells["ColButton"].Value.ToString() == "Left")
            {
                return MouseButtons.Left;
            }
            else if (dataGridView1.Rows[ntrow].Cells["ColButton"].Value.ToString() == "Right")
            {
                return MouseButtons.Right;
            }
            else
            {
                return MouseButtons.None;
            }
        }

        private Keys SetKeysCode(int ntrow)
        {
            string str = dataGridView1.Rows[ntrow].Cells["ColAction"].Value.ToString();
            switch (str)
            {
                case "A":
                    return Keys.A;
                case "B":
                    return Keys.B;
                case "C":
                    return Keys.C;
                case "D":
                    return Keys.D;
                case "E":
                    return Keys.E;
                case "F":
                    return Keys.F;
                case "G":
                    return Keys.G;
                case "H":
                    return Keys.H;
                case "I":
                    return Keys.I;
                case "J":
                    return Keys.J;
                case "K":
                    return Keys.K;
                case "L":
                    return Keys.L;
                case "M":
                    return Keys.M;
                case "N":
                    return Keys.N;
                case "O":
                    return Keys.O;
                case "P":
                    return Keys.P;
                case "Q":
                    return Keys.Q;
                case "R":
                    return Keys.R;
                case "S":
                    return Keys.S;
                case "T":
                    return Keys.T;
                case "U":
                    return Keys.U;
                case "V":
                    return Keys.V;
                case "W":
                    return Keys.W;
                case "X":
                    return Keys.X;
                case "Y":
                    return Keys.Y;
                case "Z":
                    return Keys.Z;
                case "Control":
                    return Keys.Control;
                case "Alt":
                    return Keys.Alt;
                case "Shift":
                    return Keys.Shift;
                case "LShiftKey":
                    return Keys.LShiftKey;
                case "RShiftKey":
                    return Keys.RShiftKey;
                case "0":
                    return Keys.NumPad0;
                case "1":
                    return Keys.NumPad1;
                case "2":
                    return Keys.NumPad2;
                case "3":
                    return Keys.NumPad3;
                case "4":
                    return Keys.NumPad4;
                case "5":
                    return Keys.NumPad5;
                case "6":
                    return Keys.NumPad6;
                case "7":
                    return Keys.NumPad7;
                case "8":
                    return Keys.NumPad8;
                case "9":
                    return Keys.NumPad9;
                case "F1":
                    return Keys.F1;
                case "F2":
                    return Keys.F2;
                case "F3":
                    return Keys.F3;
                case "F4":
                    return Keys.F4;
                case "F5":
                    return Keys.F5;
                case "F6":
                    return Keys.F6;
                case "F7":
                    return Keys.F7;
                case "F8":
                    return Keys.F8;
                case "F9":
                    return Keys.F9;
                case "F10":
                    return Keys.F10;
                case "F11":
                    return Keys.F11;
                case "F12":
                    return Keys.F12;
                case "PrintScreen":
                    return Keys.PrintScreen;
                case "Right":
                    return Keys.Right;
                case "Up":
                    return Keys.Up;
                case "Left":
                    return Keys.Left;
                case "Down":
                    return Keys.Down;
                case "LControlKey":
                    return Keys.LControlKey;
                case "Return":
                    return Keys.Return;
                case "RControlKey":
                    return Keys.RControlKey;
                case "Space":
                    return Keys.Space;
                case "Back":
                    return Keys.Back;
                case "Tab":
                    return Keys.Tab;
            }
            return Keys.None;
        }
    }
}
