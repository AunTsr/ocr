﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DriveAPI 
{
    class Program
    {
        private static string[] Scopes = { DriveService.Scope.Drive };
        private static string _ApplicationName = "GoogleDriveAPIStart";
        private static string _FolderID = "1G1mn2JTUec__Ek2hMYU1t9bLh9j1zyAk";
        private static string _fileName = "testfile.jpg";
        private static string _filePath = @"C:\Users\prata\Desktop\picturetest\ROI2.png";
        private static string _contentType = "image/jpeg";
        private static string _downloadFilePath = @"C:\Users\prata\source\repos\DemoOCRUnilever\DriveAPI\bin\Debug\DriveAPITest.jpg";

        static void Main(string[] args)
        {
            Console.WriteLine("Create creds");
            UserCredential credential = GetUserCredential();

            Console.WriteLine("Get Service");
            DriveService service = GetDriveService(credential);


            GetName(service);
            string flName;
            flName = Console.ReadLine();

            Console.WriteLine("Upload file");
            var fileId = UploadFileToDrive(service, _fileName, _filePath, _contentType);

            //Console.WriteLine("Download file");
            //DownloadFileFormDrive(service, flName, _downloadFilePath);

            Console.WriteLine("End");
            Console.ReadKey();
        }

        private static UserCredential GetUserCredential()
        {
            using (var stream = new FileStream("credentials.json", FileMode.Open,FileAccess.Read))
            {
                string creadPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                creadPath = Path.Combine(creadPath,"driveApiCredentials","drive-credentials.json");

                return GoogleWebAuthorizationBroker.AuthorizeAsync(GoogleClientSecrets.Load(stream).Secrets, Scopes, "User", CancellationToken.None, new FileDataStore(creadPath, true)).Result;
            }
        }

        private static DriveService GetDriveService(UserCredential credential)
        {
            return new DriveService(
                new BaseClientService.Initializer
                {
                    HttpClientInitializer = credential,
                    ApplicationName = _ApplicationName
               });
            
        }

        private static string UploadFileToDrive(DriveService service, string filename, string filePath, string contentType)
        {
            var fileMatadata = new Google.Apis.Drive.v3.Data.File();
            fileMatadata.Name = filename;
            fileMatadata.Parents = new List<string> { _FolderID };

            FilesResource.CreateMediaUpload request;
            
            using (var stream = new FileStream (filePath, FileMode.Open))
            {
                request = service.Files.Create(fileMatadata, stream, contentType);
                request.Upload();
            }
            var file = request.ResponseBody;

            return file.Id;
        }

        private static void DownloadFileFormDrive(DriveService service, string fileId, string filePath)
        {
            var request = service.Files.Get(fileId);

            using (var memoryStream = new MemoryStream())
            {
                request.MediaDownloader.ProgressChanged += (IDownloadProgress progress) =>
                {
                    switch (progress.Status)
                    {
                        case DownloadStatus.Downloading:
                            Console.WriteLine(progress.BytesDownloaded);
                            break;
                        case DownloadStatus.Completed:
                            Console.WriteLine("Download complete");
                            break;
                        case DownloadStatus.Failed:
                            Console.WriteLine("Download failed");
                            break;
                    }
                };

                request.Download(memoryStream);

                using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                {
                    fileStream.Write(memoryStream.GetBuffer(), 0, memoryStream.GetBuffer().Length);
                }
            }
        }

        private static void GetName(DriveService service)
        {
            //Define parameters of request.
            FilesResource.ListRequest listRequest = service.Files.List();
            listRequest.PageSize = 20;
            listRequest.Fields = "nextPageToken, files(id, name)";

            // List files.
            IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute()
                .Files;
            Console.WriteLine("Files:");
            if (files != null && files.Count > 0)
            {
                foreach (var file in files)
                {
                    Console.WriteLine("{0} ({1})", file.Name, file.Id);
                }
            }
            else
            {
                Console.WriteLine("No files found.");
            }
        }
    }


    //    // If modifying these scopes, delete your previously saved credentials
    //    // at ~/.credentials/drive-dotnet-quickstart.json
    //    static string[] Scopes = { DriveService.Scope.DriveReadonly };
    //    static string ApplicationName = "Drive API .NET Quickstart";

    //    static void Main(string[] args)
    //    {
    //        UserCredential credential;

    //        using (var stream =
    //            new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
    //        {
    //            // The file token.json stores the user's access and refresh tokens, and is created
    //            // automatically when the authorization flow completes for the first time.
    //            string credPath = "token.json";
    //            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
    //                GoogleClientSecrets.Load(stream).Secrets,
    //                Scopes,
    //                "user",
    //                CancellationToken.None,
    //                new FileDataStore(credPath, true)).Result;
    //            Console.WriteLine("Credential file saved to: " + credPath);
    //        }

    //        // Create Drive API service.
    //        var service = new DriveService(new BaseClientService.Initializer()
    //        {
    //            HttpClientInitializer = credential,
    //            ApplicationName = ApplicationName,
    //        });

    //        // Define parameters of request.
    //        FilesResource.ListRequest listRequest = service.Files.List();
    //        listRequest.PageSize = 10;
    //        listRequest.Fields = "nextPageToken, files(id, name)";

    //        // List files.
    //        IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute()
    //            .Files;
    //        Console.WriteLine("Files:");
    //        if (files != null && files.Count > 0)
    //        {
    //            foreach (var file in files)
    //            {
    //                Console.WriteLine("{0} ({1})", file.Name, file.Id);
    //            }
    //        }
    //        else
    //        {
    //            Console.WriteLine("No files found.");
    //        }
    //        Console.Read();

    //    }
}