﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MouseKeyboardLibrary;
using System.Threading;

namespace GlobalMacroRecorder
{
    public partial class MacroForm : Form
    {
        List<MacroEvent> events = new List<MacroEvent>();
        List<myEventRecord> events_2 = new List<myEventRecord>();

        //List<RootObject> aa = new List<RootObject>();

        int lastTimeRecorded = 0;

        MouseHook mouseHook = new MouseHook();
        KeyboardHook keyboardHook = new KeyboardHook();

        public MacroForm()
        {
            InitializeComponent();
            loadtemplatelist();

            //mouseHook.MouseMove += new MouseEventHandler(mouseHook_MouseMove);
            mouseHook.MouseDown += new MouseEventHandler(mouseHook_MouseDown);
            mouseHook.MouseUp += new MouseEventHandler(mouseHook_MouseUp);

            keyboardHook.KeyDown += new KeyEventHandler(keyboardHook_KeyDown);
            keyboardHook.KeyUp += new KeyEventHandler(keyboardHook_KeyUp);
           
        }
        //#region MouseMove
        //void mouseHook_MouseMove(object sender, MouseEventArgs e)
        //{
        //    mousemove_2(e);
        //}


        //void mousemove_1(MouseEventArgs e)
        //{
        //    events.Add(
        //        new MacroEvent(
        //            MacroEventType.MouseMove,
        //            e,
        //            Environment.TickCount - lastTimeRecorded
        //        ));

        //    lastTimeRecorded = Environment.TickCount;
        //}

        //void mousemove_2(MouseEventArgs e)
        //{
        //    events_2.Add(
        //        new myEventRecord()
        //        {
        //            MacroEventType = MacroEventType.MouseMove,
        //            EventArgs = new CustomEventArgs()
        //            {
        //                X = e.X,
        //                Y = e.Y,
        //                Button = e.Button
        //            },
        //            TimeSinceLastEvent = Environment.TickCount - lastTimeRecorded
        //        }
        //    );
        //    lastTimeRecorded = Environment.TickCount;
        //}
        //#endregion

        #region MouseDown

        void mouseHook_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown_2(e);
        }

        void mouseDown_1(MouseEventArgs e)
        {
            events.Add(
                new MacroEvent(
                    MacroEventType.MouseMove,
                    e,
                    Environment.TickCount - lastTimeRecorded
                ));

            lastTimeRecorded = Environment.TickCount;
        }

        void mouseDown_2(MouseEventArgs e)
        {
            events_2.Add(
                new myEventRecord()
                {
                    MacroEventType = MacroEventType.MouseDown,
                    EventArgs = new CustomEventArgs()
                    {
                        X = e.X,
                        Y = e.Y,
                        Button = e.Button
                    },
                    TimeSinceLastEvent = Environment.TickCount - lastTimeRecorded
                }
            );
            lastTimeRecorded = Environment.TickCount;
        }
        #endregion

        #region MouseUp

        void mouseHook_MouseUp(object sender, MouseEventArgs e)
        {
            mouseUp_2(e);
        }

        void mouseUp_1(MouseEventArgs e)
        {
            events.Add(
                new MacroEvent(
                    MacroEventType.MouseMove,
                    e,
                    Environment.TickCount - lastTimeRecorded
                ));

            lastTimeRecorded = Environment.TickCount;
        }

        void mouseUp_2(MouseEventArgs e)
        {
            events_2.Add(
                new myEventRecord()
                {
                    MacroEventType = MacroEventType.MouseUp,
                    EventArgs = new CustomEventArgs()
                    {
                        X = e.X,
                        Y = e.Y,
                        Button = e.Button
                    },
                    TimeSinceLastEvent = Environment.TickCount - lastTimeRecorded
                }
            );
            lastTimeRecorded = Environment.TickCount;
        }

        #endregion

        #region KeyDown
        void keyboardHook_KeyDown(object sender, KeyEventArgs e)
        {
            KeyDown2(e);
        }

        void KeyDown1(KeyEventArgs e)
        {
            events.Add(
            new MacroEvent(
                MacroEventType.KeyDown,
                e,
                Environment.TickCount - lastTimeRecorded
            ));

            lastTimeRecorded = Environment.TickCount;
        }

        void KeyDown2(KeyEventArgs e)
        {
            events_2.Add(
            new myEventRecord()
            {
                MacroEventType = MacroEventType.KeyDown,
                EventArgs = new CustomEventArgs()
                {
                    Keycode = e.KeyCode
                },
                TimeSinceLastEvent = Environment.TickCount - lastTimeRecorded
            }
        );

            lastTimeRecorded = Environment.TickCount;
        }
        #endregion

        #region KeyUp

        void keyboardHook_KeyUp(object sender, KeyEventArgs e)
        {
            KeyUp2(e);
            
        }

        void KeyUp1(KeyEventArgs e)
        {
            events.Add(
            new MacroEvent(
                    MacroEventType.KeyUp,
                    e,
                    Environment.TickCount - lastTimeRecorded
            ));

            lastTimeRecorded = Environment.TickCount;
        }

        void KeyUp2(KeyEventArgs e)
        {
            events_2.Add(
            new myEventRecord()
            {
                MacroEventType = MacroEventType.KeyUp,
                EventArgs = new CustomEventArgs()
                {
                    Keycode = e.KeyCode
                },
                TimeSinceLastEvent = Environment.TickCount - lastTimeRecorded
            }
        );

            lastTimeRecorded = Environment.TickCount;
        }
        #endregion

        private void recordStartButton_Click(object sender, EventArgs e)
        {

            events.Clear();
            lastTimeRecorded = Environment.TickCount;

            keyboardHook.Start();
            mouseHook.Start();

        }
        private void recordStopButton_Click(object sender, EventArgs e)
        {

            keyboardHook.Stop();
            mouseHook.Stop();

        }

        private void playBackMacroButton_Click(object sender, EventArgs e)
        {
            Replay2();
        }

        public void Replay1()
        {
            foreach (MacroEvent macroEvent in events)
            {

                Thread.Sleep(macroEvent.TimeSinceLastEvent);

                switch (macroEvent.MacroEventType)
                {
                    case MacroEventType.MouseMove:
                        {

                            MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;

                            MouseSimulator.X = mouseArgs.X;
                            MouseSimulator.Y = mouseArgs.Y;

                        }
                        break;
                    case MacroEventType.MouseDown:
                        {

                            MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;

                            MouseSimulator.MouseDown(mouseArgs.Button);

                        }
                        break;
                    case MacroEventType.MouseUp:
                        {

                            MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;

                            MouseSimulator.MouseUp(mouseArgs.Button);

                        }
                        break;
                    case MacroEventType.KeyDown:
                        {

                            KeyEventArgs keyArgs = (KeyEventArgs)macroEvent.EventArgs;

                            KeyboardSimulator.KeyDown(keyArgs.KeyCode);

                        }
                        break;
                    case MacroEventType.KeyUp:
                        {

                            KeyEventArgs keyArgs = (KeyEventArgs)macroEvent.EventArgs;

                            KeyboardSimulator.KeyUp(keyArgs.KeyCode);

                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public void Replay2 ()
        {
            foreach (myEventRecord macroEvent in events_2)
            {
                Thread.Sleep(macroEvent.TimeSinceLastEvent);

                switch (macroEvent.MacroEventType)
                {
                    case MacroEventType.MouseMove:
                        {

                            //MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;

                            MouseSimulator.X = macroEvent.EventArgs.X;
                            MouseSimulator.Y = macroEvent.EventArgs.Y;

                        }
                        break;
                    case MacroEventType.MouseDown:
                        {

                            //MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;

                            MouseSimulator.X = macroEvent.EventArgs.X;
                            MouseSimulator.Y = macroEvent.EventArgs.Y;

                            MouseSimulator.MouseDown(macroEvent.EventArgs.Button);

                        }
                        break;
                    case MacroEventType.MouseUp:
                        {

                            //MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;

                            MouseSimulator.MouseUp(macroEvent.EventArgs.Button);

                        }
                        break;
                    case MacroEventType.KeyDown:
                        {

                            //KeyEventArgs keyArgs = (KeyEventArgs)macroEvent.EventArgs;

                            KeyboardSimulator.KeyDown(macroEvent.EventArgs.Keycode);

                        }
                        break;
                    case MacroEventType.KeyUp:
                        {

                            //KeyEventArgs keyArgs = (KeyEventArgs)macroEvent.EventArgs;

                            KeyboardSimulator.KeyUp(macroEvent.EventArgs.Keycode);

                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {            
            string FileSave = textBox1.Text;
            var config = SystemData.ObjToJson(events_2);
            SystemData.SaveTxt2file(FileSave+".MaCro", config);

            if (!comboBox1.Items.Contains(textBox1.Text + ".MaCro")) comboBox1.Items.Add(textBox1.Text + ".MaCro");
            comboBox1.Text = textBox1.Text + ".MaCro";
        }

        private void Load_Click(object sender, EventArgs e)
        {
            loadconfig();
            button2.BackColor = Color.LimeGreen;
        }

        void loadtemplatelist()
        {
            var exepath = AppDomain.CurrentDomain.BaseDirectory;
            var templates = SystemData.getfileName(exepath, "*.MaCro");
            comboBox1.Items.AddRange(templates.ToArray());
        }

        private void loadconfig()
        {
            try
            {
                var configname = comboBox1.Text;
                var config = SystemData.ReadTxtfile(configname);

                events.Clear();
                events_2 = SystemData.JsontoObj<List<myEventRecord>>(config);
                //dynamic aa = SystemData.JsontoObj<dynamic>(config);

                ////events = aa as List<MacroEvent>;

                //foreach (var @event in aa)
                //{
                //   events.Add(new MacroEvent(
                //       @event[0].Value,
                //       @event[1].Value,
                //       @event[2].Value
                //       ));
                //}
                //var query = (dynamic)events.Select(o => o.EventArgs);     
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            button2.BackColor = Color.Red;
        }
    }
}
