﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GlobalMacroRecorder
{
    public class CustomEventArgs
    {
        public MouseButtons Button { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public Keys Keycode { get; set; }



        public CustomEventArgs()
        {

        }
    }

    public class myEventRecord
    {
        public MacroEventType MacroEventType { get; set; }
        public CustomEventArgs EventArgs { get; set; }
        public int TimeSinceLastEvent { get; set; }

        public myEventRecord()
        {

        }

    }

    /// <summary>
    /// All possible events that macro can record
    /// </summary>
    [Serializable]
    public enum MacroEventType
    {
        MouseMove,
        MouseDown,
        MouseUp,
        MouseWheel,
        KeyDown,
        KeyUp
    }

    /// <summary>
    /// Series of events that can be recorded any played back
    /// </summary>
    [Serializable]
    public class MacroEvent 
    {

        public MacroEventType MacroEventType;
        public EventArgs EventArgs;
        public int TimeSinceLastEvent ;

        public MacroEvent()
        {
            EventArgs = new EventArgs();
            MacroEventType = new MacroEventType();
        }

        public MacroEvent(MacroEventType macroEventType, EventArgs eventArgs, int timeSinceLastEvent)
        {
            MacroEventType = macroEventType;
            EventArgs = eventArgs;
            TimeSinceLastEvent = timeSinceLastEvent;            
        }
    }


    //public class CustomMacroEvent
    //{

    //    public MacroEventType MacroEventType;
    //    public CustomEventArgs EventArgs;
    //    public int TimeSinceLastEvent;

    //    public CustomMacroEvent()
    //    {
    //        //EventArgs = new CustomEventArgs();
    //        //MacroEventType = new MacroEventType();
    //    }

    //    public CustomMacroEvent(MacroEventType macroEventType, CustomEventArgs eventArgs, int timeSinceLastEvent)
    //    {
    //        MacroEventType = macroEventType;
    //        EventArgs = eventArgs;
    //        TimeSinceLastEvent = timeSinceLastEvent;
    //    }
    //}

    //public class Location
    //{
    //    public bool IsEmpty { get; set; }
    //    public int X { get; set; }
    //    public int Y { get; set; }
    //}

    //public class EventA
    //{
    //    public MouseButtons Button { get; set; }
    //    public int Clicks { get; set; }
    //    public int X { get; set; }
    //    public int Y { get; set; }
    //    public int Delta { get; set; }
    //    public Location Location { get; set; }
    //}

    //public class RootObject
    //{
    //    public MacroEventType MacroEventType { get; set; }
    //    public EventA EventArgs { get; set; }
    //    public int TimeSinceLastEvent { get; set; }
    //}

    public class Location
    {
        public bool IsEmpty { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public Location()
        {

        }
    }

    public enum eventType
    {
        Mouse,
        Keyboard
    }
}
//public int Clicks { get; set; }
//public int Delta { get; set; }
//public Location Location { get; set; }
//public bool? Alt { get; set; }
//public bool? Control { get; set; }
//public bool? Handled { get; set; }
//public int? KeyCode { get; set; }
//public int? KeyValue { get; set; }
//public int? KeyData { get; set; }
//public int? Modifiers { get; set; }
//public bool? Shift { get; set; }
//public bool? SuppressKeyPress { get; set; }