﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MouseKeyboardLibrary;


namespace GlobalMacroRecorder
{
    public class MyDataMacro
    {
        public MacroEventType MacroEventType { get; set; } 
        public int TimeSinceLastEvent { get; set; }
        public CustomEventArgs EventArgs { get; set; } = new CustomEventArgs();

        public MyDataMacro(MacroEventType MacroEventTy, CustomEventArgs EventAr, int TimeSinceLastEv)
        {
            MacroEventType = MacroEventTy;
            TimeSinceLastEvent = TimeSinceLastEv;
            EventArgs = EventAr;
        }

        public MyDataMacro ()
        {

        }
    }

    //public class RootObject 
    //{
    //    public MacroEventType MacroEventType { get; set; } = new MacroEventType();
    //    public MouseButtons Button { get; set; } = new MouseButtons();
    //    public int TimeSinceLastEvent { get; set; }
    //    public int X { get; set; }
    //    public int Y { get; set; }
    //    public int KeyCode { get; set; }


        //public RootObject(MacroEventType MacroEventTy, int TimeSinceLastEv, EventA EventAr)
        //{
        //    MacroEventType = MacroEventTy;
        //    TimeSinceLastEvent = TimeSinceLastEv;
        //    EventA = EventAr;
        //}
    //}

    //public class Location
    //{
    //    public bool IsEmpty { get; set; }
    //    public int X { get; set; }
    //    public int Y { get; set; }
    //}

    //public class EventA : EventArgs
    //{
    //    public int Button { get; set; }
    //    public int Clicks { get; set; }
    //    public int X { get; set; }
    //    public int Y { get; set; }
    //    public int Delta { get; set; }
    //    public Location Location { get; set; }
    //}
}



