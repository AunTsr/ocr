﻿using DemoOCRUnilever.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoOCRUnilever
{
    public static class LoadImage
    {
        public static void Fromcamera()
        {
            globalParams.Cap = new Emgu.CV.VideoCapture(0);
            globalParams.Cap.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, Int32.Parse((string)Settings.Default["Width"]));
            globalParams.Cap.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, Int32.Parse((string)Settings.Default["Height"]));
            globalParams.Cap.Read(globalParams.FrameGlobal);
        }

        public static void FromFlie()
        {
            //PdfToPng.GetPng();
            PdfToPng.GetPNG_Iron();
        }
    }
}
