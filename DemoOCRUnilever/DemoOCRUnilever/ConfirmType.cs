﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOCRUnilever
{
    public static class ConfirmType
    {
        public static bool IsTrueBox = false;

        public static bool CheckResult(string Data, string Type, int Digit, string Simbol)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(Data);
            List<int> Ints = new List<int>();

            for (int i = 0; i < bytes.Length; i++)
            {
                if (bytes[i] != 32)
                {
                    Ints.Add((int)bytes[i]);
                }
            }
            if (Digit == 0)
            {
                Selector(Data, Type, Ints);
                return IsTrueBox;
            }

            else
            {
                if (Simbol == "=")
                {
                    if (Ints.Count == Digit)
                    {
                        Selector(Data, Type, Ints);
                        return IsTrueBox;
                    }
                    return false;
                }
                else if (Simbol == ">")
                {
                    if (Ints.Count > Digit)
                    {
                        Selector(Data, Type, Ints);
                        return IsTrueBox;
                    }
                    return false;
                }
                else if (Simbol == "<")
                {
                    if (Ints.Count < Digit)
                    {
                        Selector(Data, Type, Ints);
                        return IsTrueBox;
                    }
                    return false;
                }
                return false;
            }
        }

        private static void Selector(string Data, string Type, List<int> Ints)
        {
            if (Type == "Decimal")
            {
                IsTrueBox = CheckResultDecimal(Data);
            }
            else if (Type == "Text")
            {
                IsTrueBox = CheckResultText(Ints, Ints.Count);
            }
            else if (Type == "Date")
            {
                IsTrueBox = CheckResultDate(Data);
            }
        }

        private static bool CheckResultText (List<int> Data, int Length)
        {
            int j = 0;
            for (int k = 0; k < Data.Count; k++)
            {
                //if (Data[k] >= 65 & Data[k] <= 90 || Data[k] >= 97 & Data[k] <= 122)
                //{
                    j++;
                //}
            }
            if (j == Length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool CheckResultDecimal(string Data)
        {
            decimal value;
            if (Decimal.TryParse(Data, out value))
            {
                return true;
            }
            else
            {
                return false;
            }
            #region test
            //int j = 0;
            //for (int k = 0; k < Data.Count; k++)
            //{
            //    if (Data[k] >= 48 & Data[k] <= 57)
            //    {
            //        j++;
            //    }
            //}
            //if (j == Length)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            #endregion
        }

        private static bool CheckResultDate(string Data)
        {
            DateTime dDate;
            if (DateTime.TryParse(Data, out dDate))
            {
                //String.Format("{0:d/MM/yyyy}", dDate);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}        