﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoOCRUnilever
{
    public partial class frmMassagebox : Form
    {
        public frmMassagebox()
        {
            InitializeComponent();
        }

        private void FrmMassagebox_Load(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            globalParams.myAns = 1;
            globalParams.waitAns = true;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            globalParams.myAns = 2;
            globalParams.waitAns = true;
        }
    }
}
