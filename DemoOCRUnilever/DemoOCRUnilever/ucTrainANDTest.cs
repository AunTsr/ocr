﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.ML;
using DemoOCRUnilever.Properties;
using System.Web.Script.Serialization;
using System.IO;

namespace DemoOCRUnilever
{
    public partial class ucTrainANDTest : UserControl
    {
        new Size size = new Size(100, 35);
        List<string> a = new List<string>();
        Mat matTestingNumbersGen = new Mat();
        Mat matGrayscaleGen = new Mat();
        Mat matBlurredGen = new Mat();
        Mat matThreshGen = new Mat();
        Mat matThreshCopyGen = new Mat();
        Mat Img = new Mat();
        Mat classes = new Mat();

        int i = 0;
        int j = 0;

        public ucTrainANDTest()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Mat matTestingNumbers = new Mat();
            Mat matGrayscale = new Mat();
            Mat matBlurred = new Mat(); ;
            Mat matThresh = new Mat(); ;
            Mat matThreshCopy = new Mat();
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat hier = new Mat();
            Mat outImg = new Mat();

            Mat classifications = new Mat();
            Mat images = new Mat();
            Mat images1 = new Mat();
            Mat images2 = new Mat();

            string jj = textBox3.Text;
            matTestingNumbers = CvInvoke.Imread(jj);
            CvInvoke.CvtColor(matTestingNumbers, matGrayscale, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
            CvInvoke.GaussianBlur(matGrayscale, matBlurred, new Size(5, 5), 0);
            CvInvoke.AdaptiveThreshold(matBlurred, matThresh, 255, Emgu.CV.CvEnum.AdaptiveThresholdType.GaussianC, Emgu.CV.CvEnum.ThresholdType.BinaryInv, 11, 2);

            //matThreshCopy = matThresh.Clone();
            matThresh.ConvertTo(matThreshCopy, Emgu.CV.CvEnum.DepthType.Cv32F);
            CvInvoke.Resize(matThreshCopy, matThreshCopy, size, 0, 0, Emgu.CV.CvEnum.Inter.Linear);
            //CvInvoke.FindContours(matThreshCopy, contours, hier, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            //CvInvoke.DrawContours(outImg, contours, -1, new MCvScalar(0,255,0));

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            FileStorage aaa = new FileStorage("classifications.xml", FileStorage.Mode.Read);
            if (!aaa.IsOpened)
            {                                    // if the file was not opened successfully
                Console.Write("BaGaBaGaBaGa");   // show error message                                                                                 // and exit program
            }

            using (FileNode node = aaa.GetNode("classifications"))
            {
                node.ReadMat(classifications);
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            FileStorage bbb = new FileStorage("images.xml", FileStorage.Mode.Read);
            if (!bbb.IsOpened)
            {                                    // if the file was not opened successfully
                Console.Write("BaGaBaGaBaGa");   // show error message                                                                                 // and exit program
            }

            using (FileNode node = bbb.GetNode("images"))
            {
                node.ReadMat(images);
                images.ConvertTo(images1, Emgu.CV.CvEnum.DepthType.Cv32F);
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            var configImgs = ReadTxtfile("Text.txt");
            a = JsontoObj<List<string>>(configImgs);

            using (KNearest knn = new KNearest())
            {
                knn.Train(images1, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, classifications);
                //knn.Save("Test");
                //knn.Load("Test");
                Mat dd = new Mat(0, 0, Emgu.CV.CvEnum.DepthType.Cv32F, 1);
                images2 = matThreshCopy.Reshape(1, 1);
                knn.FindNearest(images2, 1, dd);
                int result = (int)dd.GetValue(0, 0);
                textBox4.Text = a[result].ToString();
                //textBox4.Text = result.ToString();
                //float ee = (float)dd.Ptr;
                //float dd = (float)cc.SetTo<float>(0);
                //int k = 0;
                //knn.DefaultK = k;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Settings.Default.Reset();
            //const int MIN_CONTOUR_AREA_GEN = 15;
            //const int RESIZED_IMAGE_WIDTH_GEN = 14;
            //const int RESIZED_IMAGE_HEIGHT_GEN = 20;
            //VectorOfVectorOfPoint contoursGen = new VectorOfVectorOfPoint();
            //for (int j = 0; j < a.Count; j++)
            //{
            //    Settings.Default["a" + j] = a[j];
            //}
            //Settings.Default.Save();

            var ImgSave = ObjToJson(Img);
            SaveTxt2file("Imgs.txt", ImgSave);
            var ClassesSave = ObjToJson(classes);
            SaveTxt2file("Classes.txt", ClassesSave);
            var TextSave = ObjToJson(a);
            SaveTxt2file("Text.txt", TextSave);

            FileStorage fileImg = new FileStorage("images.xml", FileStorage.Mode.Write);
            FileStorage fileClass = new FileStorage("classifications.xml", FileStorage.Mode.Write);

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        

            fileImg.Write(Img, "images");
            fileImg.ReleaseAndGetString();

            #region note
            //Matrix<byte> matrix = new Matrix<byte>(1,1);
            //matrix.Data[0, 0] = 1;
            //mat.SetTo(new MCvScalar(i),matrix);
            //Mat mat = new Mat();
            //mat = CvInvoke.Imread("Original.jpg");
            #endregion

            fileClass.Write(classes, "classifications");
            fileClass.ReleaseAndGetString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //openFileDialog1.ShowDialog();
                textBox1.Text = openFileDialog1.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (j==0)
            {
                if (JsontoObj<Mat>(ReadTxtfile("Imgs.txt")) != null)
                {
                    var configImgs = ReadTxtfile("Imgs.txt");
                    Img = JsontoObj<Mat>(configImgs);
                }

                if (JsontoObj<Mat>(ReadTxtfile("Classes.txt")) != null)
                {
                    var configImgs = ReadTxtfile("Classes.txt");
                    classes = JsontoObj<Mat>(configImgs);
                }

                if (JsontoObj<List<string>>(ReadTxtfile("Text.txt")) != null)
                {
                    var configImgs = ReadTxtfile("Text.txt");
                    a = JsontoObj<List<string>>(configImgs);
                }
            }

            string Imgs = textBox1.Text;
            matTestingNumbersGen = CvInvoke.Imread(Imgs);
            //CvInvoke.Imshow("ccc", matTestingNumbersGen);
            CvInvoke.CvtColor(matTestingNumbersGen, matGrayscaleGen, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
            CvInvoke.GaussianBlur(matGrayscaleGen, matBlurredGen, new Size(5, 5), 0);
            CvInvoke.AdaptiveThreshold(matBlurredGen, matThreshGen, 255, Emgu.CV.CvEnum.AdaptiveThresholdType.GaussianC, Emgu.CV.CvEnum.ThresholdType.BinaryInv, 11, 2);

            matThreshCopyGen = matThreshGen.Clone();
            CvInvoke.Resize(matThreshCopyGen, matThreshCopyGen, size, 0, 0, Emgu.CV.CvEnum.Inter.Linear);
            //CvInvoke.Imshow("AAA", matThreshCopyGen);
            matThreshCopyGen = matThreshCopyGen.Reshape(1, 1);
            Img.PushBack(matThreshCopyGen);

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            string str = textBox2.Text;
            a.Add(str);
            Mat mat = new Mat(1, 1, Emgu.CV.CvEnum.DepthType.Cv32S, 1);
            mat.SetValue(0, 0, i);
            classes.PushBack(mat);
            i++;
            j++;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog2 = new OpenFileDialog();
            if (openFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //openFileDialog1.ShowDialog();
                textBox3.Text = openFileDialog2.FileName;
            }
        }

        #region save  template

        public static T JsontoObj<T>(string json)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            T obj = js.Deserialize<T>(json);

            return obj;
        }

        public static string ObjToJson<T>(T obj)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            var output = js.Serialize(obj);
            return output;
        }

        public static bool SaveTxt2file(string filepath, string txt)
        {
            try
            {
                filepath = AppDomain.CurrentDomain.BaseDirectory + filepath;
                File.WriteAllText(filepath, txt);
                return true;
            }
            catch
            {

                return false;
            }
        }

        public static string ReadTxtfile(string filepath)
        {
            try
            {
                filepath = AppDomain.CurrentDomain.BaseDirectory + filepath;
                return File.ReadAllText(filepath);
            }
            catch
            {
                return "";
            }
        }
        #endregion
    }
}
