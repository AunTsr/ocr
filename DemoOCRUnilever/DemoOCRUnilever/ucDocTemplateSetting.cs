﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Emgu.CV;
using System.Threading;
using System.IO;
using System.Linq;
using System.Data;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Web.Script.Serialization;
using DemoOCRUnilever.Properties;
using Microsoft.VisualBasic;

namespace DemoOCRUnilever
{
    public partial class ucDocTemplateSetting : UserControl
    {
        List<MyData> TextBoxFrm = new List<MyData>();  
        Mat FrameVideo = new Mat();
        Mat FrameTemp = new Mat();
        Mat MatTemp = new Mat();
        Point StartLocation;
        Point EndLocation;
        bool IsThreadActive = true;
        bool IsMouseDown = false;
        int i = 0;
        int j = 0;
        Rectangle rect = new Rectangle();
        List<Mat> TT = new List<Mat>();
        List<string> ComboboxData = new List<string>();
        bool IsVedioShow = true;

        public ucDocTemplateSetting()
        {
            InitializeComponent();
            loadtemplatelist();
        }

        #region select position
        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            i = j;
            button3.BackColor = System.Drawing.Color.Transparent;
            IsMouseDown = true;
            StartLocation = e.Location;
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsMouseDown == true)
            {
                EndLocation = e.Location;
                imageBox1.Invalidate();
            }
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (rect != null)
            {
                e.Graphics.DrawRectangle(Pens.Red, GetRectangle());
            }
        }

        private Rectangle GetRectangle()
        {
            rect.X = Math.Min(StartLocation.X, EndLocation.X);
            rect.Y = Math.Min(StartLocation.Y, EndLocation.Y);
            rect.Width = Math.Abs(StartLocation.X - EndLocation.X);
            rect.Height = Math.Abs(StartLocation.Y - EndLocation.Y);
            return rect;
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (IsMouseDown == true)
            {
                EndLocation = e.Location;
                IsMouseDown = false;
                if (rect != null)
                {
                    Image<Bgr, byte> imgeOrigenal = FrameTemp.ToImage<Bgr, byte>();
                    imgeOrigenal.ROI = rect;
                    Image<Bgr, byte> temp = imgeOrigenal.CopyBlank();
                    imgeOrigenal.CopyTo(temp);
                    imgeOrigenal.ROI = Rectangle.Empty;
                    MatTemp = temp.Mat;
                }
            }
        }
        #endregion
        #region Vedio
        private void CameraShow_Click(object sender, EventArgs e)
        {
            LoadImage.Fromcamera();
            IsThreadActive = true;
            IsVedioShow = true;
            Thread thread1 = new Thread(new ThreadStart(VedioCamera));
            thread1.Start();
        }

        public void VedioCamera()
        {
            while (IsThreadActive)
            {
                Video();
                Thread.Sleep(1000);
                Application.DoEvents();
            }
            globalParams.Cap.Stop();
        }

        private void Video()
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action(Video), null);
                return;
            }
            if (IsVedioShow==true)
            {
                globalParams.Cap.Read(globalParams.FrameGlobal);
                CvInvoke.Resize(globalParams.FrameGlobal, FrameVideo, new Size(640, 480)/*PictureBox.Size*/, 0, 0, Emgu.CV.CvEnum.Inter.Linear);
                imageBox1.Image = FrameVideo;
            }

        }
        #endregion

        private void LoadImgFromFile_Click(object sender, EventArgs e)
        {
            IsThreadActive = false;
            LoadImage.FromFlie();
            globalParams.FrameRotate = globalParams.FrameGlobal;
            imageBox1.Image = globalParams.FrameRotate;
            FrameTemp = globalParams.FrameGlobal;
        }

        private void SaveImg_Click(object sender, EventArgs e)
        {
            IsVedioShow = false;
            string DeleteFilepart = AppDomain.CurrentDomain.BaseDirectory + "imgOriginal.png";
            if (File.Exists(DeleteFilepart))
            {
                File.Delete(DeleteFilepart);
            }
            imageBox1.Image = globalParams.FrameGlobal;
            FrameTemp = globalParams.FrameGlobal;
            CvInvoke.Imwrite("ImgOriginal.png", globalParams.FrameGlobal);
        }

        private void Rotate_Click(object sender, EventArgs e)
        {
            if (IsThreadActive == true)
            {
                globalParams.FrameGlobal.CopyTo(globalParams.FrameRotate);
                IsThreadActive = false;
            }
            CvInvoke.Rotate(globalParams.FrameRotate, globalParams.FrameRotate, Emgu.CV.CvEnum.RotateFlags.Rotate90Clockwise);
            FrameTemp = globalParams.FrameRotate;
            imageBox1.Image = FrameTemp;
        }

        private void CropROI_Click(object sender, EventArgs e)
        {
            string TypeOfText = comboBox2.Text;
            string digit = textBox1.Text;
            string Simbol = comboBox3.Text;
            string header = textBox2.Text;
            button3.BackColor = Color.LimeGreen;
            IsThreadActive = false;
            string DeleteFile = AppDomain.CurrentDomain.BaseDirectory + "ROI" + i + ".png";
            if (File.Exists(DeleteFile))
            {
                File.Delete(DeleteFile);
            }
            CvInvoke.Imwrite("ROI" + i + ".png", MatTemp);
            //CvInvoke.Resize(MatTemp, MatTemp, new Size(240, 33), 0, 0, Emgu.CV.CvEnum.Inter.Linear);
            TextBoxFrm.Add(new MyData {rects = rect, DataTypes = TypeOfText, Digits = int.Parse(digit), Simbols = Simbol, Header = header });
            globalParams.WW.Add(rect);
            globalParams.DataType.Add(TypeOfText);
            globalParams.IntDigit.Add(int.Parse(digit));
            globalParams.HeaderGlobal.Add(header);
            TT.Add(MatTemp);
            showRecAll();
            j++;
            MatTemp.Dispose();
        }

        private void showRecAll()
        {
            Mat temp = new Mat();
            FrameTemp.CopyTo(temp);
            for (int k = 0; k < globalParams.WW.Count; k++)
            {
                Rectang(ref temp, globalParams.WW[k], new Bgr(Color.LimeGreen).MCvScalar, k.ToString());
            }
            imageBox1.Image = temp;
        }

        public Bitmap Rectang(ref Mat mat, Rectangle rec, MCvScalar cl, string desc = "")
        {
            if (mat == null) return null;
            CvInvoke.Rectangle(mat, rec, cl, thickness: 2);
            if (desc != "") CvInvoke.PutText(mat, desc, new Point(rec.Location.X, rec.Location.Y), FontFace.HersheyTriplex, 1, cl);
            return null;
        }

        private void ClearFrom_Click(object sender, EventArgs e)
        {
            TextBoxFrm.Clear();
            TT.Clear();
            globalParams.WW.Clear();
            i = 0;
            j = 0;
            imageBox1.Image = FrameTemp;
        }

        #region save  template
        private void SaveFrom_Click(object sender, EventArgs e)
        {
            savetemplate(comboBox1.Text);
        }   

        void Savejson(string name)
        {
            var testjsonsave = SystemData.ObjToJson(TextBoxFrm);
            //SaveTxt2file(globalParams.TemplateName, testjsonsave);
            SystemData.SaveTxt2file(name, testjsonsave);
        }

        void SaveComboboxData()
        {
            var textcombo = SystemData. ObjToJson(ComboboxData);
            SystemData.SaveTxt2file("ComboboxData.txt", textcombo);
        }
        #endregion

        private void NewFrom_Click(object sender, EventArgs e)
        {
            var newtemplate  = Interaction.InputBox("", "Templatename");

            if (newtemplate.Trim() != "")
            {
                savetemplate(newtemplate + ".ocrtemplate");
            }
        }

        void savetemplate(string templatename)
        {
            Savejson(templatename);
            if(!comboBox1.Items.Contains(templatename)) comboBox1.Items.Add(templatename);
            comboBox1.Text = templatename;
            globalParams.WW.Clear();
        }

        void loadtemplatelist()
        {
            var exepath = AppDomain.CurrentDomain.BaseDirectory;
            var templates = SystemData.getfileName(exepath, "*.ocrtemplate");
            comboBox1.Items.AddRange(templates.ToArray());
        }


        void loadconfig()
        {
            try
            {
                var configname = comboBox1.Text;
                var config = SystemData.ReadTxtfile(configname);
                TextBoxFrm = SystemData.JsontoObj<List<MyData>>(config);
                var query = TextBoxFrm.Select(o => o.rects);
                globalParams.WW = query.Cast<Rectangle>().ToList();
                var query2 = TextBoxFrm.Select(t => t.DataTypes);
                globalParams.DataType = query2.Cast<string>().ToList();
                var query3 = TextBoxFrm.Select(d => d.Digits);
                globalParams.IntDigit = query3.Cast<int>().ToList();
                var query4 = TextBoxFrm.Select(s => s.Simbols);
                globalParams.SimbolGlobal = query4.Cast<string>().ToList();
                var query5 = TextBoxFrm.Select(h => h.Header);
                globalParams.HeaderGlobal = query5.Cast<string>().ToList();
                WriteRectanglePicture();
            }
            catch (Exception)
            {

            }           
        }

        private void WriteRectanglePicture ()
        {
            Mat Mat1 = new Mat();
            globalParams.FrameGlobal.CopyTo(Mat1);

            for (int k = 0; k < globalParams.WW.Count; k++)
            {
                Rectang(ref Mat1, globalParams.WW[k], new Bgr(Color.LimeGreen).MCvScalar, k.ToString());

                string DeleteFile = AppDomain.CurrentDomain.BaseDirectory + "ROI" + k + ".png";
                if (File.Exists(DeleteFile))
                {
                    File.Delete(DeleteFile);
                }
                Image<Bgr, byte> imgeOrigenal = globalParams.FrameGlobal.ToImage<Bgr, byte>();
                imgeOrigenal.ROI = globalParams.WW[k];
                Image<Bgr, byte> tempImage = imgeOrigenal.CopyBlank();
                imgeOrigenal.CopyTo(tempImage);
                imgeOrigenal.ROI = Rectangle.Empty;
                MatTemp = tempImage.Mat;
                CvInvoke.Imwrite("ROI" + k + ".png", MatTemp);
                CvInvoke.Resize(MatTemp, MatTemp, new Size(240, 33), 0, 0, Emgu.CV.CvEnum.Inter.Linear);
                TT.Add(MatTemp);
                //MatTemp.Dispose();
            }
            imageBox1.Image = Mat1;
            imageBox1.Size = new Size(640, 480);
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            loadconfig();
        }

        private void Undo_Click(object sender, EventArgs e)
        {
            if (j - 1 >= 0)
            {
                TextBoxFrm.RemoveAt(j - 1);
                TT.RemoveAt(j - 1);
                globalParams.WW.RemoveAt(j - 1);
                WriteRectanglePicture();
                j = j - 1;
            }
        }

        private void ucDocTemplateSetting_Load(object sender, EventArgs e)
        {

        }
    }
}
