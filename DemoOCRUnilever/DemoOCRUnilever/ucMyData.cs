﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoOCRUnilever
{
    public partial class ucMyData : UserControl
    {
        Rectangle a = new Rectangle();
        string b;
        int c;
        List<MyData> datas = new List<MyData>();
        List<Rectangle> aa = new List<Rectangle>();

        public ucMyData()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            a.X = int.Parse(X.Text);
            a.Y = int.Parse(Y.Text);
            a.Width = int.Parse(Width.Text);
            a.Height = int.Parse(Height.Text);
            b = comboBox1.Text;
            c = int.Parse(Digit.Text);
            datas.Add(new MyData { rects = a,DataTypes=b,Digits=c });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var ee = SystemData.ObjToJson(datas);
            var dd = SystemData.JsontoObj<List<MyData>>(ee);
            var query2 = datas.Select(o => o.rects );
            aa = query2.Cast<Rectangle>().ToList();         
        }
    }
}
