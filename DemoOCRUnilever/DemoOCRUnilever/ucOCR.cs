﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using DemoOCRUnilever.Properties;
using System.Data;
using System.Linq;
using System.Threading;
using Tesseract;
using System.ComponentModel;
using Microsoft.VisualBasic;
using TheNew;

namespace DemoOCRUnilever
{
    public partial class ucOCR : UserControl
    {
        DataTable dataTable = new DataTable();
        List<Rectangle> WW = new List<Rectangle>();
        List<Mat> TT = new List<Mat>();
        bool IsThreadActive = true;
        Mat FrameVideo = new Mat();
        Mat FrameTemp = new Mat();
        Mat a = new Mat();
        List<string> ResultS = new List<string>();
        List<bool> IsTrues = new List<bool>();
        string OcrText = "";
        List<MyData> TextBoxFrm = new List<MyData>();
        List<MyGridView> myGridViews = new List<MyGridView>();
        bool IsVedioShow = true;
        bool IsTrue = false;
        char[] charsForTrim = { '*', ' ', '\n', ':' };
        int z=0;
        string path;

        public ucOCR()
        {
            InitializeComponent();
            loadtemplatelist();
        }

        private void LoadFrm_Click(object sender, EventArgs e)
        {
            //IsThreadActive = false;
            var configname = comboBox1.Text;
            var config = SystemData.ReadTxtfile(configname);
            TextBoxFrm = SystemData.JsontoObj<List<MyData>>(config);
            var query = TextBoxFrm.Select(o => o.rects);
            globalParams.WW = query.Cast<Rectangle>().ToList();
            var query2 = TextBoxFrm.Select(t => t.DataTypes);
            globalParams.DataType = query2.Cast<string>().ToList();
            var query3 = TextBoxFrm.Select(d => d.Digits);
            globalParams.IntDigit = query3.Cast<int>().ToList();
            var query4 = TextBoxFrm.Select(s => s.Simbols);
            globalParams.SimbolGlobal = query4.Cast<string>().ToList();
            var query5 = TextBoxFrm.Select(h => h.Header);
            globalParams.HeaderGlobal = query5.Cast<string>().ToList();

            Mat MatTemp = new Mat();
            Mat Mat1 = new Mat();
            FrameTemp.CopyTo(Mat1);

            for (int k = 0; k < globalParams.WW.Count; k++)
            {
                Rectang(ref Mat1, globalParams.WW[k], new Bgr(Color.LimeGreen).MCvScalar, k.ToString());
                ////CvInvoke.Rectangle(temp, WW[k], new Bgr(Color.Red).MCvScalar, 1);

                string DeleteFile = AppDomain.CurrentDomain.BaseDirectory + "ROI" + k + ".png";
                if (File.Exists(DeleteFile))
                {
                    File.Delete(DeleteFile);
                }
                Image<Bgr, byte> imgeOrigenal = Mat1.ToImage<Bgr, byte>();
                imgeOrigenal.ROI = globalParams.WW[k];
                Image<Bgr, byte> tempImage = imgeOrigenal.CopyBlank();
                imgeOrigenal.CopyTo(tempImage);
                imgeOrigenal.ROI = Rectangle.Empty;
                MatTemp = tempImage.Mat;
                CvInvoke.Imwrite("ROI" + k + ".png", MatTemp);
                CvInvoke.Resize(MatTemp, MatTemp, new Size(240, 33), 0, 0, Emgu.CV.CvEnum.Inter.Linear);
                TT.Add(MatTemp);
                //MatTemp.Dispose();
            }
            imageBox1.Image = Mat1;
            imageBox1.Size = new Size(640, 480);
        }

        public Bitmap Rectang(ref Mat mat, Rectangle rec, MCvScalar cl, string desc = "")
        {
            if (mat == null) return null;
            CvInvoke.Rectangle(mat, rec, cl, thickness: 2);
            if (desc != "") CvInvoke.PutText(mat, desc, new Point(rec.Location.X, rec.Location.Y), FontFace.HersheyTriplex, 1, cl);

            //imgbox.Image = mat;
            return null;
        }

        //private void button3_Click(object sender, EventArgs e)
        //{
        //    var config = ReadTxtfile("ComboboxData.txt");
        //    globalParams.ComboboxDaata = JsontoObj<List<string>>(config);
        //    for (int i =0; i< globalParams.ComboboxDaata.Count;i++ )
        //    {
        //        comboBox1.Items.Add(globalParams.ComboboxDaata[i]);
        //    }
        //}

        private void CameraOpen_Click(object sender, EventArgs e)
        {
            LoadImage.Fromcamera();
            IsThreadActive = true;
            IsVedioShow = true;
            Thread thread1 = new Thread(new ThreadStart(VedioCamera));
            thread1.Start();
        }

        public void VedioCamera()
        {
            while (IsThreadActive)
            {
                Video();
                Thread.Sleep(1000);
                Application.DoEvents();
            }
            globalParams.Cap.Stop();
        }

        private void Video()
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action(Video), null);
                return;
            }
            if (IsVedioShow == true)
            {
                globalParams.Cap.Read(globalParams.FrameGlobal);
                CvInvoke.Resize(globalParams.FrameGlobal, FrameVideo, new Size(640, 480)/*PictureBox.Size*/, 0, 0, Emgu.CV.CvEnum.Inter.Linear);
                imageBox1.Image = FrameVideo;
            }

        }

        #region Zoom picture
        public Bitmap ScaleByPercent(Bitmap imgPhoto, int Percent)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            var destWidth = (int)(sourceWidth * nPercent);
            var destHeight = (int)(sourceHeight * nPercent);

            var bmPhoto = new Bitmap(destWidth, destHeight,
                                     System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                                  imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                              new System.Drawing.Rectangle(0, 0, destWidth, destHeight),
                              new System.Drawing.Rectangle(0, 0, sourceWidth, sourceHeight),
                              GraphicsUnit.Pixel);

            Image<Bgr, Byte> imageCVPower = new Image<Bgr, byte>(bmPhoto); //Image Class from Emgu.CV
            Mat matpower = imageCVPower.Mat; //This is your Image converted to Mat

            CvInvoke.Imwrite("TO.png", matpower);
            //bmPhoto.Save(@"D:\Scale.png", System.Drawing.Imaging.ImageFormat.Png);
            grPhoto.Dispose();
            return bmPhoto;
        }
        #endregion

        private void ImgOpen_Click(object sender, EventArgs e)
        {
            IsThreadActive = false;
            LoadImage.FromFlie();
            globalParams.FrameRotate = globalParams.FrameGlobal;
            //Bitmap bitmap = globalParams.FrameRotate.Bitmap;
            //imageBox1.Image = ConvertImage.BitmapToMat(ScaleByPercent(bitmap,25));
            imageBox1.Image = globalParams.FrameRotate;
            FrameTemp = globalParams.FrameGlobal;
        }

        private void CapANDRotate_Click(object sender, EventArgs e)
        {
            if (IsThreadActive == true)
            {
                globalParams.FrameGlobal.CopyTo(globalParams.FrameRotate);
                IsThreadActive = false;
            }
            CvInvoke.Rotate(globalParams.FrameRotate, globalParams.FrameRotate, Emgu.CV.CvEnum.RotateFlags.Rotate90Clockwise);
            FrameTemp = globalParams.FrameRotate;
            imageBox1.Image = FrameTemp;
        }

        private void OCR_Click(object sender, EventArgs e)
        {
            path = AppDomain.CurrentDomain.BaseDirectory + "test1.xlsx";
            //fnReadWriteExcel.keydata2exxcel(path,null);
            //return;

            textBox1.Text = "";
            OcrText = "";
            for (int k = 0; k < globalParams.WW.Count; k++)
            {
                Bitmap img = new Bitmap("ROI" + k + ".png");//This is your bitmap
                TesseractEngine OCR = new TesseractEngine("./tessdata", "eng", EngineMode.Default);
                //OCR.SetVariable("tessedit_unrej_any_wd", "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
                //OCR.SetVariable("tessedit_char_blacklist", "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"); 
                //OCR.SetVariable("tessedit_char_whitelist", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
                Page page = OCR.Process(ScaleByPercent(img, 100));
                string Result = page.GetText();
                string ResultC = Result.Trim(charsForTrim);
                IsTrue = ConfirmType.CheckResult(ResultC, globalParams.DataType[k], globalParams.IntDigit[k], globalParams.SimbolGlobal[k]);
                ResultS.Add(ResultC);
                IsTrues.Add(IsTrue);
                //myGridViews.Add(new MyGridView() { Header = globalParams.HeaderGlobal[k], Data = ResultC });
                OcrText = OcrText + Result + "\r\n";
                //dataTable.Columns.Add(globalParams.HeaderGlobal[k],typeof(string));
                //dataTable.Rows.Add();
                
                if (z==0)
                {
                    DataGridViewTextBoxColumn dt = new DataGridViewTextBoxColumn();
                    dt.HeaderText = globalParams.HeaderGlobal[k];
                    dataGridView1.Columns.Add(dt);
                }
                //ResultShow = ResultShow + Result + "\r\n";
                img.Dispose(); //close bitmap for to use anathor
            }

            dataGridView1.Rows.Add();
            for (int i=0; i< globalParams.HeaderGlobal.Count; i++)
            {
                if (!IsTrues[i])
                {
                    dataGridView1.Rows[z].Cells[i].Style.ForeColor = Color.Red;
                }
                else
                {
                    dataGridView1.Rows[z].Cells[i].Style.ForeColor = Color.Black;
                }
                dataGridView1.Rows[z].Cells[i].Value = ResultS[i];
            }
            z++;
            //fnReadWriteExcel.DataGrid2Excel(dataGridView1);
            //var bindingList = new BindingList<MyGridView>(myGridViews);
            //var source = new BindingSource(bindingList, null);
            //dataGridView1.DataSource = dataTable;
            //dataGridView1.Columns[0].HeaderText = globalParams.HeaderGlobal[0];
            ResultS.Clear();
            IsTrues.Clear();
            //fnReadWriteExcel.WriteRowColExcel(0, 0, ResultS, 1, path, IsTrue);
            textBox1.Text = OcrText;
        }

        //public DataTable ConvertToDataTable<T>(IList<T> data)
        //{
        //    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
        //    DataTable table = new DataTable();
        //    foreach (PropertyDescriptor prop in properties)
        //    {
        //        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        //    }
        //    foreach (T item in data)
        //    {
        //        DataRow row = table.NewRow();
        //        foreach (PropertyDescriptor prop in properties)
        //            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
        //        table.Rows.Add(row);
        //    }
        //    return table;
        //}

        void savetemplate(string templatename)
        {
            Savejson(templatename);
            if (!comboBox1.Items.Contains(templatename)) comboBox1.Items.Add(templatename);
            comboBox1.Text = templatename;
            WW.Clear();
        }

        void loadtemplatelist()
        {
            var exepath = AppDomain.CurrentDomain.BaseDirectory;
            var templates = SystemData.getfileName(exepath, "*.ocrtemplate");
            comboBox1.Items.AddRange(templates.ToArray());
        }

        void Savejson(string name)
        {
            var testjsonsave = SystemData.ObjToJson(WW);
            SystemData.SaveTxt2file(name, testjsonsave);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                fnReadWriteExcel.DataGrid2Excel(dataGridView1);
                //fnReadWriteExcel.keydata2exxcel(path, ResultS, IsTrues, globalParams.HeaderGlobal);
            }
            catch { }
        }

        private void ComboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count != 0 & dataGridView1.Rows != null)
            {
                int i;
                frmMassagebox aa = new frmMassagebox();
                aa.Show();
                while (!globalParams.waitAns)
                {
                    Thread.Sleep(100);
                    Application.DoEvents();

                    if (globalParams.myAns ==1)
                    {
                        fnReadWriteExcel.DataGrid2Excel(dataGridView1);
                    }
                    else if (globalParams.myAns ==2)
                    {
                        i = 50;
                    }
                    else
                    {
                        //i = 100;
                    }                
                }
                z = 0;
                dataGridView1.Rows.Clear();
                dataGridView1.Columns.Clear();
                globalParams.waitAns = false;
                globalParams.myAns = 0;
                aa.Close();
            }

        //    if (dataGridView1 != null)
        //    {
        //        var newtemplate = Interaction.InputBox("", "Templatename");

        //        if (newtemplate.Trim() != "")
        //        {
        //            savetemplate(newtemplate + ".ocrtemplate");
        //        }
        //    }
        }
    }
}
