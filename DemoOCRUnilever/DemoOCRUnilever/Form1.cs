﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;

namespace DemoOCRUnilever
{
    public partial class Form1 : Form
    {
        enum pages
        {
            Home,
            Ocr,
            Trian
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadPage(pages.Home);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            loadPage(pages.Ocr);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            loadPage(pages.Trian);
        }

        void loadPage(pages p)
        {
            panel1.Controls.Clear();

            switch (p)
            {
                case pages.Home:
                    panel1.Controls.Add(new ucHome());
                    break;
                case pages.Ocr:
                    panel1.Controls.Add(new ucOCR());
                    break;
                case pages.Trian:
                    panel1.Controls.Add(new ucTrainANDTest());
                    break;
            }
            panel1.Controls[0].Dock = DockStyle.Fill;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            VideoCapture a = new VideoCapture(0);
            Mat b = new Mat();
            Mat img = new Mat();
            a.Read(b);
            //Image<Bgr, byte> b = new Image<Bgr, byte>("imgOriginal.png");
            Bitmap c = b.Bitmap;
            FindAngle d = new FindAngle();
            img = d.DeskewImage(c,1,1);
            CvInvoke.Imshow("ImgRotate", img);
            CvInvoke.Imshow("OriginalImg", b);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string data = "Aa";
            string type = "Text";
            ConfirmType.CheckResult(data, type,2,"=");
        }
    }
}
