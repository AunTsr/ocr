﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOCRUnilever
{
    public static class globalParams
    {
        public static VideoCapture Cap ;
        public static Mat FrameGlobal = new Mat();
        public static Mat FrameRotate = new Mat();
        public static string TemplateName;
        public static List<string> ComboboxDaata = new List<string>();
        public static List<Rectangle> WW = new List<Rectangle>();
        public static List<string> DataType = new List<string>();
        public static List<int> IntDigit = new List<int>();
        public static List<string> SimbolGlobal = new List<string>();
        public static List<string> HeaderGlobal = new List<string>();
        public static bool waitAns = false ;
        public static int myAns = 0 ;
    }
}
