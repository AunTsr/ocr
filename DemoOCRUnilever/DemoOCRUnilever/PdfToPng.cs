﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IronPdf;
using IronSoftware;
using IronOcr;
using System.IO;

namespace DemoOCRUnilever
{
    public static class PdfToPng
    {
        public static void GetPng()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "(PDF Only)|*.pdf";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string PdfFile = openFileDialog1.FileName;
                    string PngFile = "Convert.png";
                    //PdfToImage.PDFConvert pdfConvert = new PdfToImage.PDFConvert();
                    //pdfConvert.Convert(PdfFile, PngFile, "300");
                    cs_pdf_to_image.Pdf2Image.PrintQuality = 100;
                    List<string> Convertion = cs_pdf_to_image.Pdf2Image.Convert(PdfFile, PngFile);
                    Bitmap Output = new Bitmap(PngFile);
                    globalParams.FrameGlobal = ConvertImage.BitmapToMat(Output);
                    //CvInvoke.Imwrite("TT.png", img);
                    //CvInvoke.Imshow("Test", img);   
                }
                catch (Exception E)
                {
                    try
                    {
                        VideoCapture a = new VideoCapture(openFileDialog1.FileName);
                        a.Read(globalParams.FrameGlobal);
                    }
                    catch
                    {
                        MessageBox.Show(E.Message);
                    }
                }
            }
        }


        public static void GetPNG_Iron()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "(PDF Only)|*.pdf";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string PdfFile = openFileDialog1.FileName;
                    PdfDocument pdf = new PdfDocument(PdfFile, null);

                    var path = Directory.GetParent(PdfFile);
                    var filename = Path.GetFileNameWithoutExtension(PdfFile);
                    var imagesave = $"{path}\\{filename}.jpg";
                    pdf.ToJpegImages(imagesave);
                    VideoCapture a = new VideoCapture(imagesave);
                    a.Read(globalParams.FrameGlobal);
                }
                catch (Exception E)
                {

                }
            }                     
        }
    }
}
