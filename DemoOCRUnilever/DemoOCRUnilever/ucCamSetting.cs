﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using DemoOCRUnilever.Properties;

namespace DemoOCRUnilever
{
    public partial class ucCamSetting : UserControl
    {
        int WidthInt;
        int HeightInt;

        public ucCamSetting()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            {
                string combobox = comboBox1.Text;
                string Width = "";
                string Heigh = "";
                char[] chr = combobox.ToCharArray();
                int j = 0;
                for (int i = 0; i < chr.Length; i++)
                {
                    if (chr[i] != 'X')
                    {
                        if (j == 0)
                        {
                            Width = Width + chr[i];
                        }
                        else
                        {
                            Heigh = Heigh + chr[i];
                        }
                    }
                    else
                    {
                        j++;
                    }
                }
                j = 0;
                WidthInt = Int32.Parse(Width);
                HeightInt = Int32.Parse(Heigh);
                Settings.Default["Width"] = WidthInt.ToString();
                Settings.Default["Height"] = HeightInt.ToString();

                VideoCapture camVatical = new VideoCapture(0);
                camVatical.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, WidthInt);
                camVatical.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, HeightInt);
                //globalParams.Cap = camVatical;
            }
        }
    }
}
