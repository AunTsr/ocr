﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoOCRUnilever
{
    public partial class ucHome : UserControl
    {
        public ucHome()
        {
            InitializeComponent();
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            tabPage1.Controls.Add(new ucCamSetting());
            tabPage2.Controls.Add(new ucDBSetting());
            tabPage3.Controls.Add(new ucDocTemplateSetting());
        }

        private void tabPage3_ControlAdded(object sender, ControlEventArgs e)
        {
            e.Control.Dock = DockStyle.Fill;
        }
    }
}
