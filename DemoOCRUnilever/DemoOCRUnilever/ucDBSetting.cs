﻿using System;
using System.Windows.Forms;
using DemoOCRUnilever.Properties;

namespace DemoOCRUnilever
{
    public partial class ucDBSetting : UserControl
    {
        string ServerName ;
        string DatabaseName ;
        string UserID ;
        string Password ;
        string Timeout ;

        public ucDBSetting()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ServerName = txtbx_server.Text;
            DatabaseName = txtbx_database.Text;
            UserID = txtbx_user.Text;
            Password = txtbx_password.Text;
            Timeout = txtbx_timeout.Text;
            Settings.Default.ServerName = ServerName;
            Settings.Default.DatabaseName = DatabaseName;
            Settings.Default.UserID = UserID;
            Settings.Default.Password = Password;
            Settings.Default.Timeout = Timeout;
            Settings.Default.Save();
        }

        private void ucDBSetting_Load(object sender, EventArgs e)
        {
            ServerName = (string)Settings.Default.ServerName;
            DatabaseName = (string)Settings.Default.DatabaseName;
            UserID = (string)Settings.Default.UserID;
            Password = (string)Settings.Default.Password;
            Timeout = (string)Settings.Default.Timeout;
        }
    }
}
