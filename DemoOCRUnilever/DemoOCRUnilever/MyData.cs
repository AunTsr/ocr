﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOCRUnilever
{
    public class MyData
    {
        public Rectangle rects { get; set; }
        public string DataTypes  { get; set; }
        public int Digits { get; set; }
        public string Simbols { get; set; }
        public string Header { get; set; }
    }

    public class MyGridView
    {
        public string Header { get; set; }
        public string Data { get; set; }
    }
}
