﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Testusechart
{
    public partial class AUNChart : Form
    {
        public AUNChart()
        {
            InitializeComponent();
        }

        public void showChart()
        {
            var Order = chart2.Series["Order"];
            var Error = chart2.Series["Err."];

            Order.Points.Clear();
            Error.Points.Clear();

            Order.Points.AddXY(0, 50);
            Error.Points.AddXY(0, 2);

            Order.Points.AddXY(1, 50);
            Error.Points.AddXY(1, 2);

            Order.Points.AddXY(2, 50);
            Error.Points.AddXY(2, 2);

            Order.Points.AddXY(3, 50);
            Error.Points.AddXY(3, 2);

            Order.Points.AddXY(4, 50);
            Error.Points.AddXY(4, 2);

            Order.Points.AddXY(5, 50);
            Error.Points.AddXY(5, 2);

            chart2.Legends[0].Enabled = true;
            chart2.Series[0].IsValueShownAsLabel = true;
            chart2.Series[1].IsValueShownAsLabel = true;
            chart2.ChartAreas[0].AxisX.Interval = 1;
        }

        public void addChart(DataSet dataSet )
        {
            int z = 0;
            for (int i = 0; i < dataSet.Tables.Count; i++)
            {
                z = 0;
                for (int j = 0; j < dataSet.Tables[i].Rows.Count; j++)
                {
                    for (int k = 0; k < dataSet.Tables[i].Columns.Count; k++)
                    {
                        if ( z == 0 )
                        {
                            chart2.Series.Add(dataSet.Tables[i].Columns[k].ColumnName);
                        }
                        
                        Series a = new Series();
                        a = chart2.Series[dataSet.Tables[i].Columns[k].ColumnName];
                        a.Points.AddXY( i +1, dataSet.Tables[i].Rows[j][k]);

                        //chart2.Series[k].Enabled = true;
                        chart2.Series[k].IsValueShownAsLabel = true;
                    }
                    z++;
                }
            }

            chart2.Legends[0].Enabled = true;
            chart2.ChartAreas[0].AxisX.Interval = 1;

        }

        public void addChart()
        {
            for (int i = 0; i < 5; i++)
            {
                if (i == 0)
                {
                    chart2.Series.Add("111" + i + "plc10");
                    Series a = new Series();
                    a = chart2.Series["111" + i + "plc10"];
                    a.Points.AddXY(0, i + 10);
                    a.Points.AddXY(1, i + 10);
                    a.Points.AddXY(2, i + 10);
                    a.Points.AddXY(3, i + 10);
                }
                else if (i == 1)
                {
                    chart2.Series.Add("111" + i + "plc10");
                    Series a = new Series();
                    a = chart2.Series["111" + i + "plc10"];
                    a.Points.AddXY(0, i + 10);
                    a.Points.AddXY(2, i + 10);
                    a.Points.AddXY(3, i + 10);
                }
                else if (i == 2)
                {
                    chart2.Series.Add("111" + i + "plc10");
                    Series a = new Series();
                    a = chart2.Series["111" + i + "plc10"];
                    a.Points.AddXY(0, i + 10);
                    a.Points.AddXY(1, i + 10);
                    a.Points.AddXY(3, i + 10);
                }
                else if (i == 3)
                {
                    chart2.Series.Add("111" + i + "plc10");
                    Series a = new Series();
                    a = chart2.Series["111" + i + "plc10"];
                    a.Points.AddXY(0, i + 10);
                    a.Points.AddXY(1, i + 10);
                    a.Points.AddXY(2, i + 10);
                }
                else if (i == 4)
                {
                    chart2.Series.Add("111" + i + "plc10");
                    Series a = new Series();
                    a = chart2.Series["111" + i + "plc10"];
                    a.Points.AddXY(0, i + 10);
                }
                chart2.Series[i].IsValueShownAsLabel = true;
            }
            chart2.Legends[0].Enabled = true;
            chart2.ChartAreas[0].AxisX.Interval = 1;
        }
    }
}
