﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Testusechart
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            AUNChart chart = new AUNChart();
            chart.showChart();
            chart.Show();

        }

        private void Button2_Click(object sender, EventArgs e)
        {

            DataTable table = GetTable();
            DataTable table2 = GetTable2();

            DataSet set = new DataSet("Test");
            set.Tables.Add(table);
            set.Tables.Add(table2);

            AUNChart b = new AUNChart();

            b.addChart(set);
            //b.addChart();
            b.Show();

            //chart1.Show();  

            //System.IO.MemoryStream myStream = new System.IO.MemoryStream();
            //chart1.Serializer.Save(myStream);

            //new AUNChart().addChart(chart1);
        }

        public static DataTable GetTable()
        {
            // Here we create a DataTable with four columns.
            DataTable table = new DataTable();
            table.Columns.Add("plc10", typeof(int));
            table.Columns.Add("plc11", typeof(int));
            table.Columns.Add("plc12", typeof(int));
            table.Columns.Add("plc13", typeof(int));
            table.Columns.Add("plc14", typeof(int));
            table.Columns.Add("plc15", typeof(int));

            // Here we add five DataRows.
            table.Rows.Add(1, 2, 3, 4, 5, 6);
            table.Rows.Add(1, 2, 3, 4, 5, 6);
            table.Rows.Add(1, 2, 3, 4, 5, 6);
            return table;
        }

        public static DataTable GetTable2()
        {
            // Here we create a DataTable with four columns.
            DataTable table = new DataTable();
            table.Columns.Add("plc20", typeof(int));
            table.Columns.Add("plc21", typeof(int));

            // Here we add five DataRows.
            table.Rows.Add(1, 2);
            table.Rows.Add(1, 2);
            table.Rows.Add(1, 2);
            return table;
        }
    }
}
